#version 410

layout(location = 0) out vec4 fragColor;

uniform sampler2D tex;

in VS_OUT {
    vec2 texcoord;
} fs_in;

void main()
{
    vec4 difftex = texture(tex, fs_in.texcoord);
    if (difftex.a < 0.5) discard;
    fragColor = vec4(vec3(gl_FragCoord.z), 1.0);
    
}
