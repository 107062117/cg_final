#version 410

layout(location = 0) in vec3 iv3vertex;

uniform mat4 um4m;
uniform mat4 um4mvp_shadow;


void main()
{
    gl_Position = um4mvp_shadow * vec4(iv3vertex, 1.0);

}
