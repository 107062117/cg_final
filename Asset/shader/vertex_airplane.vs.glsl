#version 410

layout(location = 0) in vec3 vertex;
layout (location = 1) in vec3 normal;
layout(location = 2) in vec2 texcoord;

out VS_OUT {
    vec3 N;
    vec3 L;
    vec3 L2;
    vec3 V;
    vec2 texcoord;
    vec4 texcoord_shadow;
    /* defer */
    vec3 position;
    vec3 normal;
} vs_out;

out float v_depth;

uniform mat4 mvpMat;
uniform mat4 um4m;
uniform mat4 um4v;
uniform mat4 um4p;
uniform mat4 um4shadow;
uniform vec4 plane;
vec3 light_dir = vec3(0.2, 0.6, 0.5);
/* point light */
vec3 point_light_pos = vec3(636.48, 134.79, 495.98);

void main()
{
    vec4 worldPos = um4m * vec4(vertex, 1.0);
    gl_ClipDistance[0] = dot(worldPos, plane);
    vs_out.texcoord = texcoord;
    
    /* phong shading */
    vec4 vm = um4v * um4m * vec4(vertex, 1.0);
    vs_out.N = mat3(um4v * um4m) * normal;
    vs_out.L = mat3(um4v) * light_dir;
    vs_out.L2 = mat3(um4v) * (point_light_pos - worldPos.xyz);
    vs_out.V = -vm.xyz;
    vs_out.texcoord_shadow = um4shadow * vec4(vertex, 1.0);
    
    gl_Position = um4p * vm;
    v_depth = gl_Position.z;
    /* defer */
    vec4 P = vec4(vertex, 1.0);
    vs_out.position = normalize((um4m * P).xyz);
    vs_out.normal = normalize(mat3(um4m) * normal);
}
