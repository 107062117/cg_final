#version 410

layout(location = 0) in vec2 iv3vertex;
layout(location = 1) in vec2 iv2tex_coord;


out VertexData
{
    vec2 texcoord;
} vertexData;

void main()
{
    vertexData.texcoord = iv2tex_coord;
    gl_Position = vec4(iv3vertex.x, iv3vertex.y, -1.0, 1.0);
}
