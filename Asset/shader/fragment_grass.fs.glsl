#version 410

layout(location = 0) out vec4 fragColor;
/* defer */
layout (location = 1) out vec4 frag_vertex;
layout (location = 2) out vec4 frag_normal;
layout (location = 3) out vec4 frag_ambient;
layout (location = 4) out vec4 frag_diffuse;
layout (location = 5) out vec4 frag_specular;
/* bloom */
layout (location = 6) out vec4 frag_bloom;

in VS_OUT {
    vec3 N;
    vec3 L;
    vec3 L2;
    vec3 V;
    vec2 texcoord;
    /* defer */
    vec3 position;
    vec3 normal;
} fs_in;

in float v_depth;
uniform int renderBlur;
uniform int pointLightOpen;

uniform sampler2D tex;
/* defer */
uniform vec4 Ka;
uniform vec4 Kd;
uniform vec4 Ks;

//vec4 ambient_albedo = vec4(0.3f, 0.3f, 0.3f, 1.0f);
//vec4 diffuse_albedo = vec4(0.5f, 0.5f, 0.5f, 1.0f);
//vec4 specular_albedo = vec4(1.0f, 1.0f, 1.0f, 1.0f);
vec4 ambient_albedo = vec4(0.1f, 0.1f, 0.1f, 1.0f);
vec4 diffuse_albedo = vec4(0.8f, 0.8f, 0.8f, 1.0f);
vec4 specular_albedo = vec4(0.1f, 0.1f, 0.1f, 1.0f);
float specular_power = 32.0; // shininess

void main()
{
    /* DOF */
    float blur = 0;

    float near_distance = 300.0;
    float far_distance = 300.0;

    float near_plane = 0.0;
    float far_plane = 800.0;

    if(v_depth <= near_plane && v_depth >= far_plane)
    {
        blur = 0;
    }
    else if(v_depth > near_plane)
    {
        blur = clamp(v_depth, near_plane, near_plane + near_distance);
        blur = (blur - near_plane) / near_distance;
    }
    else if(v_depth < far_plane)
    {
        blur = clamp(v_depth, far_plane - far_distance, far_plane);
        blur = (far_plane - blur) / far_distance;
    }
    
    /* shadow */
    vec3 N = normalize(fs_in.N);
    vec3 L = normalize(fs_in.L);
    vec3 V = normalize(fs_in.V);
    vec3 R = reflect(-L, N);
    
    vec4 difftex = texture(tex, fs_in.texcoord);
    if (difftex.a < 0.5) discard;
    
    vec4 ambient = difftex * ambient_albedo;
    vec4 diffuse = difftex * diffuse_albedo * max(dot(N, L), 0.0);
    vec4 specular = specular_albedo * pow(max(dot(R, V), 0.0), specular_power);
    
    /* point light */
    vec3 L2 = normalize(fs_in.L2);
    vec3 R2 = reflect(-L2, N);
    
    vec4 diffuse2 = difftex * diffuse_albedo * max(dot(N, L2), 0.0);
    vec4 specular2 = specular_albedo * pow(max(dot(R2, V), 0.0), specular_power);
    
    
    if (renderBlur == 1)
    {
        if (pointLightOpen == 1) fragColor = vec4(1.2 * (ambient.xyz + diffuse.xyz + specular.xyz) + (diffuse2.xyz + specular2.xyz), blur);
        else fragColor = vec4(1.2 * (ambient.xyz + diffuse.xyz + specular.xyz), blur);
    }
        
    else
    {
        if (pointLightOpen == 1) fragColor = 1.2 * (ambient + diffuse + specular) + (diffuse2 + specular2);
        else fragColor = 1.2 * (ambient + diffuse + specular);
    }
    
    /* defer */
    frag_vertex = vec4(fs_in.position, 1.0);
    frag_normal = vec4(fs_in.normal, 1.0);
    frag_ambient = vec4(Ka);
    frag_diffuse = vec4(difftex);
    frag_specular = vec4(Ks);
    /* bloom */
    frag_bloom = vec4(0.0, 0.0, 0.0, 1.0);
}
