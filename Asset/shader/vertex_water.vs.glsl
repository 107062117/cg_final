#version 410

uniform mat4 gWorld;
uniform mat4 gProj;
uniform mat4 gCamera;

layout (location = 0) in vec3 inPosition;
layout (location = 1) in vec3 inNormal;
layout (location = 2) in vec2 inTextureCoord;

out DATA {
    vec4 worldPosition;
    vec2 textureCoord;
    vec3 normal;
    vec3 toCamera;
    vec3 fromLight;
} Out;
out float v_depth;

uniform float textureTiling;
uniform vec3 cameraPosition;
vec3 light_dir = vec3(0.2, 0.6, 0.5);

void main() {
    vec4 worldPosition = gWorld * vec4(inPosition, 1.0);

    Out.worldPosition = gProj* gCamera * worldPosition;
    Out.textureCoord = inTextureCoord * textureTiling;
    Out.normal = inNormal;
    Out.toCamera = cameraPosition - worldPosition.xyz;
    Out.fromLight = -1 * light_dir;

    gl_Position = Out.worldPosition;
    v_depth = gl_Position.z;
}
