#version 410

layout(location = 0) in vec3 iv3vertex;
layout(location = 2) in vec2 iv2texcoord;
layout(location = 3) in vec3 instancePos;

uniform mat4 um4m;
uniform mat4 um4vp_shadow;


out VS_OUT {
    vec2 texcoord;
} vs_out;
void main()
{
    vs_out.texcoord = iv2texcoord;
    mat4 T = mat4(1.0, 0.0, 0.0, 0.0, 0.0, 1.0, 0.0, 0.0, 0.0, 0.0, 1.0, 0.0, instancePos.x, instancePos.y, instancePos.z, 1.0);
    gl_Position = um4vp_shadow * T * um4m * vec4(iv3vertex, 1.0);
}
