#version 410

layout(location = 0) out vec4 fragColor;

uniform sampler2D tex;

in VertexData
{
    vec2 texcoord;
} vertexData;

void main()
{
    vec4 originalColor = texture(tex, vertexData.texcoord);
    
    /* blur */
    int half_size = 4;
    vec4 color_sum = vec4(0);
    for (int i = -half_size; i <= half_size ; ++i)
    {
        for (int j = -half_size; j <= half_size ; ++j)
        {
            ivec2 coord = ivec2(gl_FragCoord.xy) + ivec2(i, j);
            vec4 blurColor = texelFetch(tex, coord, 0);
            
            color_sum.xyz += (originalColor.rgb * (1 - originalColor.a) + blurColor.rgb * originalColor.a);
        }
    }
    color_sum.w = 1.0;
    int sample_count = (half_size * 2 + 1) * (half_size * 2 + 1);

    fragColor = color_sum / sample_count;

}
