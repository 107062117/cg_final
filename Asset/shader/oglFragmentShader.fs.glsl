#version 410

in vec3 f_viewVertex ;
in vec3 f_uv ;
/* shadow */
/* phong shading */
in vec3 inN;
in vec3 inL;
/* point light */
in vec3 inL2;
in vec3 inV;
in vec4 texcoord_shadow;
/* DOF */
in float v_depth;

/* defer */
in DEFER_OUT {
    vec3 position;
    vec3 normal;
    // [Note]: texCoord: use f_uv
} defer_in;

layout (location = 0) out vec4 fragColor0;
/* defer */
layout (location = 1) out vec4 frag_vertex;
layout (location = 2) out vec4 frag_normal;
layout (location = 3) out vec4 frag_ambient;
layout (location = 4) out vec4 frag_diffuse;
layout (location = 5) out vec4 frag_specular;
/* bloom */
layout (location = 6) out vec4 frag_bloom;

uniform sampler2D texture0 ;
uniform sampler2DShadow tex_shadow;
uniform int renderBlur;
uniform int pointLightOpen;
/* defer */
uniform vec4 Ka;
uniform vec4 Kd;
vec4 Ks = vec4(0.0, 0.0, 0.0, 1.0);
//
//vec4 ambient_albedo = vec4(0.3f, 0.3f, 0.3f, 1.0f);
//vec4 diffuse_albedo = vec4(0.5f, 0.5f, 0.5f, 1.0f);
//vec4 specular_albedo = vec4(1.0f, 1.0f, 1.0f, 1.0f);
vec4 ambient_albedo = vec4(0.1f, 0.1f, 0.1f, 1.0f);
vec4 diffuse_albedo = vec4(0.8f, 0.8f, 0.8f, 1.0f);
vec4 specular_albedo = vec4(0.1f, 0.1f, 0.1f, 1.0f);
float specular_power = 32.0; // shininess
float shadow_factor;

vec4 withFog(vec4 color){
    const vec4 FOG_COLOR = vec4(1.0, 1.0, 1.0, 1) ;
    const float MAX_DIST = 1000.0 ;
    const float MIN_DIST = 600.0 ;
    
    float dis = length(f_viewVertex) ;
    float fogFactor = (MAX_DIST - dis) / (MAX_DIST - MIN_DIST) ;
    fogFactor = clamp(fogFactor, 0.0, 1.0) ;
    fogFactor = fogFactor * fogFactor ;
    
    vec4 colorWithFog = mix(FOG_COLOR, color, fogFactor) ;
    return colorWithFog ;
}

void renderTerrain(){
    
    /* DOF */
    float blur = 0;

    float near_distance = 300.0;
    float far_distance = 300.0;

    float near_plane = 0.0;
    float far_plane = 800.0;

    if(v_depth <= near_plane && v_depth >= far_plane)
    {
        blur = 0;
    }
    else if(v_depth > near_plane)
    {
        blur = clamp(v_depth, near_plane, near_plane + near_distance);
        blur = (blur - near_plane) / near_distance;
    }
    else if(v_depth < far_plane)
    {
        blur = clamp(v_depth, far_plane - far_distance, far_plane);
        blur = (far_plane - blur) / far_distance;
    }
    
    // get terrain color
    vec4 terrainColor = texture(texture0, f_uv.rg) ;
    // apply fog
    //vec4 fColor = withFog(terrainColor) ;
    // not fog
    vec4 fColor = terrainColor;
//    fColor.a = blur;
//    fragColor0 = fColor ;
    
    /* shadow */
    vec3 N = normalize(inN);
    vec3 L = normalize(inL);
    vec3 V = normalize(inV);
    vec3 R = reflect(-L, N);
        
    vec4 ambient = fColor * ambient_albedo;
    vec4 diffuse = fColor * diffuse_albedo * max(dot(N, L), 0.0);
    vec4 specular = specular_albedo * pow(max(dot(R, V), 0.0), specular_power);
    
    /* point light */
    vec3 L2 = normalize(inL2);
    vec3 R2 = reflect(-L2, N);
    
    vec4 diffuse2 = fColor * diffuse_albedo * max(dot(N, L2), 0.0);
    vec4 specular2 = specular_albedo * pow(max(dot(R2, V), 0.0), specular_power);
    
    shadow_factor = textureProj(tex_shadow, texcoord_shadow);
    if (shadow_factor < 0.2) shadow_factor = 0.2;
    else if (shadow_factor > 1.0) shadow_factor = 1.0;
    
    if (renderBlur == 1)
    {
        if (pointLightOpen == 1) fragColor0 = vec4(1.2 * (ambient.xyz + (diffuse.xyz + specular.xyz) * shadow_factor) + (diffuse2.xyz + specular2.xyz), blur);
        else fragColor0 = vec4(1.2 * (ambient.xyz + (diffuse.xyz + specular.xyz) * shadow_factor), blur);
    }
        
    else
    {
        if (pointLightOpen == 1) fragColor0 = vec4(1.2 * (ambient.xyz + (diffuse.xyz + specular.xyz) * shadow_factor) + (diffuse2.xyz + specular2.xyz), 1);
        else fragColor0 = vec4(1.2 * (ambient.xyz + (diffuse.xyz + specular.xyz) * shadow_factor), 1);
    }

    /* defer */
    frag_vertex = vec4(defer_in.position, 1.0);
    frag_normal = vec4(defer_in.normal, 1.0);
    frag_ambient = vec4(terrainColor);
    frag_diffuse = vec4(terrainColor);
    frag_specular = vec4(Ks);
    fragColor0 = ambient + (diffuse + specular) * shadow_factor;
    /* bloom */
    frag_bloom = vec4(0.0, 0.0, 0.0, 1.0);
}

void main(){
    renderTerrain() ;
}
