#version 410

layout(location = 0) out vec4 fragColor;

uniform sampler2D sceneTex;
uniform sampler2D blurTex;

in VertexData
{
    vec2 texcoord;
} vertexData;

void main()
{
    fragColor = texture(sceneTex, vertexData.texcoord) + texture(blurTex, vertexData.texcoord);
    
//    fragColor = vec4(0.0,1.0,0.0,1.0); // + texture(blurTex, vertexData.texcoord);
}
