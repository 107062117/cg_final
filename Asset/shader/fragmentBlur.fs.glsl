#version 410

layout(location = 0) out vec4 fragColor;

in VertexData
{
    vec2 texcoord;
} vertexData;

uniform sampler2D tex;

void main()
{
    vec3 texColor = texture(tex,vertexData.texcoord).rgb;
    fragColor = vec4(texColor, 1.0);
    int half_size = 7;
    vec4 color_sum = vec4(0);
    for (int i = -half_size; i <= half_size ; ++i) {
        for (int j = -half_size; j <= half_size ; ++j) {
            ivec2 coord = ivec2(gl_FragCoord.xy) + ivec2(i, j);
            color_sum += texelFetch(tex, coord, 0);
        }
    }
    int sample_count = (half_size * 2 + 1) * (half_size * 2 + 1);
    fragColor = color_sum / sample_count;
}
