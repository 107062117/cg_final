#version 410

layout (location = 0) in vec3 v_vertex;
layout (location = 1) in vec3 v_normal ;
layout (location = 2) in vec3 v_uv ;

out vec3 f_viewVertex ;
out vec3 f_uv;
/* phong shading */
out vec3 inN;
out vec3 inL;
/* point light */
out vec3 inL2;
out vec3 inV;
/* shadow coor */
out vec4 texcoord_shadow;
/* DOF */
out float v_depth;
/* defer */
out DEFER_OUT {
    vec3 position;
    vec3 normal;
    // [Note]: texCoord: use f_uv
} defer_out;

uniform mat4 projMat ;
uniform mat4 viewMat ;
uniform mat4 modelMat ;
/* shadow */
vec3 light_dir = vec3(0.2, 0.6, 0.5);
/* point light */
vec3 point_light_pos = vec3(636.48, 134.79, 495.98);

uniform mat4 um4shadow;
/* Water */
uniform vec4 plane;

// terrain
uniform mat4 vToHeightUVMat;
uniform sampler2D elevationMap;
uniform sampler2D normalMap;

vec3 getTerrainVertex(vec4 worldV){
    float mHeight = 300.0 ;
    vec4 uvForHeight = vToHeightUVMat * worldV;
    float h = texture(elevationMap, vec2(uvForHeight.x, uvForHeight.z)).r ;
    float y =  h * mHeight ;
    return vec3(worldV.x, y, worldV.z) ;
}

void renderTerrain(){
    vec4 worldV = modelMat * vec4(v_vertex, 1.0) ;
    worldV.w = 1.0;
    vec3 cVertex = getTerrainVertex(worldV) ;
    
    // get normal
    vec4 uvForNormal = vToHeightUVMat * worldV;
    vec4 normalTex = texture(normalMap, vec2(uvForNormal.x, uvForNormal.z)) ;
    // [0, 1] -> [-1, 1]
    normalTex = normalTex * 2.0 - 1.0 ;
        
    vec4 viewVertex = viewMat * vec4(cVertex, 1) ;
    vec4 viewNormal = viewMat * vec4(normalTex.rgb, 0) ;
    
    f_viewVertex = viewVertex.xyz ;
    
    vec4 uvForDiffuse = vToHeightUVMat * worldV;
    f_uv = vec3(uvForDiffuse.x, uvForDiffuse.z, 1.0) ;
    
    /* phong shading */
    vec4 vm = viewMat * vec4(cVertex, 1);
    inN = viewNormal.xyz;
    inL = mat3(viewMat) * light_dir;
    inL2 = mat3(viewMat) * (point_light_pos - cVertex);
    inV = -vm.xyz;

    /* shadow */
    texcoord_shadow = um4shadow * vec4(cVertex, 1.0);
    
    /**for water**/
    vec4 worldPos = vec4(cVertex, 1.0);
    gl_ClipDistance[0] = dot(worldPos, plane);
    
    gl_Position = projMat * viewVertex ;
    v_depth = gl_Position.z;
    /* defer */
    vec4 P = vec4(v_vertex, 1.0);
    defer_out.position = normalize(vec4(cVertex, 1).xyz);
    defer_out.normal = vec4(normalTex.rgb, 0).xyz;
}

void main(){
    
    /****/
    renderTerrain() ;
}
