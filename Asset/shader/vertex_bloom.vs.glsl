#version 410

layout(location = 0) in vec3 iv3vertex;
layout (location = 1) in vec3 iv3normal;
layout(location = 2) in vec2 iv2texcoord;

out VS_OUT {
    vec2 texcoord;
    /* defer */
    vec3 position;
    vec3 normal;
} vs_out;
out float v_depth;

uniform mat4 m_mat;
uniform mat4 v_mat;
uniform mat4 p_mat;


void main()
{
    vs_out.texcoord = iv2texcoord;
    gl_Position = p_mat * v_mat * m_mat * vec4(iv3vertex, 1.0);
    
    /* defer */
    vec4 P = vec4(iv3vertex, 1.0);
    vs_out.position = normalize((m_mat * P).xyz);
    vs_out.normal = normalize(mat3(m_mat) * iv3normal);
    v_depth = gl_Position.z;
}
