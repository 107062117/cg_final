#version 410

layout(location = 0) out vec4 fragColor;
uniform sampler2D tex;


in VertexData
{
    vec2 texcoord;
} vertexData;

void main()
{
    fragColor = texture(tex, vertexData.texcoord);
}
