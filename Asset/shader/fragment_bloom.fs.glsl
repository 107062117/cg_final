#version 410

layout(location = 0) out vec4 fragColor;
/* defer */
layout (location = 1) out vec4 frag_vertex;
layout (location = 2) out vec4 frag_normal;
layout (location = 3) out vec4 frag_ambient;
layout (location = 4) out vec4 frag_diffuse;
layout (location = 5) out vec4 frag_specular;
/* bloom */
layout (location = 6) out vec4 frag_bloom;

in VS_OUT {
    vec2 texcoord;
    /* defer */
    vec3 position;
    vec3 normal;
} fs_in;
in float v_depth;
/* defer */
vec4 Ka = vec4(1.0, 1.0, 1.0, 1.0);
vec4 Kd = vec4(0.8, 0.8, 0.8, 1.0);
vec4 Ks = vec4(0.5f, 0.5f, 0.5f, 1.0);
uniform int renderBlur;

void main()
{
    
    /* DOF */
    float blur = 0;

    float near_distance = 300.0;
    float far_distance = 300.0;

    float near_plane = 0.0;
    float far_plane = 800.0;

    if(v_depth <= near_plane && v_depth >= far_plane)
    {
        blur = 0;
    }
    else if(v_depth > near_plane)
    {
        blur = clamp(v_depth, near_plane, near_plane + near_distance);
        blur = (blur - near_plane) / near_distance;
    }
    else if(v_depth < far_plane)
    {
        blur = clamp(v_depth, far_plane - far_distance, far_plane);
        blur = (far_plane - blur) / far_distance;
    }
    if (renderBlur == 1)
        fragColor = vec4(1.0, 1.0, 1.0, blur);
    else
        fragColor = vec4(1.0, 1.0, 1.0, 1.0);
    
    
    /* defer */
    frag_vertex = vec4(fs_in.position, 1.0);
    frag_normal = vec4(fs_in.normal, 1.0);
    frag_ambient = Ka;
    frag_diffuse = vec4(1.0, 1.0, 1.0, 1.0);
    frag_specular = Ks;
    /* bloom */
    frag_bloom = vec4(1.0, 1.0, 1.0, 1.0);
}
