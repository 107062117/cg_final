#version 410 core
                                                                          
layout (location = 0) in vec3 position;
layout (location = 1) in vec3 normal;
layout (location = 2) in vec3 tangent;
layout (location = 3) in vec2 texcoord;


out VS_OUT
{
    vec3 N;
    vec3 L;
    vec3 L2;
    vec3 V;
    vec4 texcoord_shadow;
    vec2 texcoord;
    vec3 FragPos;
    vec3 TangentLightDir;
    vec3 TangentViewPos;
    vec3 TangentFragPos;
    /* defer */
    vec3 position;
    vec3 normal;
} vs_out;

out float v_depth;
                                                                          
uniform mat4 m_matrix;
uniform mat4 v_matrix;
uniform mat4 p_matrix;
uniform vec3 view_pos;
uniform vec4 plane;
/* shadow */
uniform mat4 um4shadow;
vec3 light_dir = vec3(0.2, 0.6, 0.5);
                                                                          
/* point light */
vec3 point_light_pos = vec3(636.48, 134.79, 495.98);

void main(void)
{
    vec4 worldPos = m_matrix * vec4(position, 1.0);
    gl_ClipDistance[0] = dot(worldPos, plane);
    vs_out.FragPos = vec3(m_matrix * vec4(position, 1.0));
    vs_out.texcoord = texcoord;
    
    mat3 normalMatrix = transpose(inverse(mat3(m_matrix)));
    vec3 T = normalize(normalMatrix * tangent);
    vec3 N = normalize(normalMatrix * normal);
    T = normalize(T - dot(T, N) * N);
    vec3 B = cross(N, T);
    
    mat3 TBN = transpose(mat3(T, B, N));
    vs_out.TangentLightDir = TBN * light_dir;
    vs_out.TangentViewPos  = TBN * view_pos;
    vs_out.TangentFragPos  = TBN * vs_out.FragPos;
    
    /* phong shading */
    vec4 vm = v_matrix * m_matrix * vec4(position, 1.0);
    vs_out.N = mat3(v_matrix * m_matrix) * normal;
    vs_out.L = mat3(v_matrix) * light_dir;
    vs_out.L2 = mat3(v_matrix) * (point_light_pos - worldPos.xyz);
    vs_out.V = -vm.xyz;
    vs_out.texcoord_shadow = um4shadow * vec4(position, 1.0);
        
    gl_Position = p_matrix * v_matrix * m_matrix * vec4(position, 1.0);
    v_depth = gl_Position.z;
    /* defer */
    vec4 P = vec4(position, 1.0);
    vs_out.position = normalize((m_matrix * P).xyz);
    vs_out.normal = normalize(mat3(m_matrix) * normal);
}

