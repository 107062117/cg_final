#version 410

layout(location = 0) in vec3 iv3vertex;
layout (location = 1) in vec3 iv3normal;
layout(location = 2) in vec2 iv2texcoord;
layout(location = 3) in vec3 instancePos;

out VS_OUT {
    vec3 N;
    vec3 L;
    vec3 L2;
    vec3 V;
    vec2 texcoord;
    vec4 texcoord_shadow;
    /* defer */
    vec3 position;
    vec3 normal;
} vs_out;

out float v_depth;

uniform mat4 m_mat;
uniform mat4 v_mat;
uniform mat4 p_mat;
uniform mat4 um4shadow;
uniform vec4 plane;
vec3 light_dir = vec3(0.2, 0.6, 0.5);
/* point light */
vec3 point_light_pos = vec3(636.48, 134.79, 495.98);

void main()
{
    vs_out.texcoord = iv2texcoord;
    
    mat4 T = mat4(1.0, 0.0, 0.0, 0.0, 0.0, 1.0, 0.0, 0.0, 0.0, 0.0, 1.0, 0.0, instancePos.x, instancePos.y, instancePos.z, 1.0);
    vec4 worldPos = T * m_mat *  vec4(iv3vertex, 1.0);
    gl_ClipDistance[0] = dot(worldPos, plane);
    
    /* phong shading */
    vec4 vm = v_mat * worldPos;
    vs_out.N = mat3(v_mat * T * m_mat) * iv3normal;
    vs_out.L = mat3(v_mat) * light_dir;
    vs_out.L2 = mat3(v_mat) * (point_light_pos - worldPos.xyz);
    vs_out.V = -vm.xyz;
    vs_out.texcoord_shadow = um4shadow * T * m_mat * vec4(iv3vertex, 1.0);
    
    // Final output
    gl_Position = p_mat * vm;
    v_depth = gl_Position.z;
    /* defer */
    vs_out.position = normalize(worldPos.xyz);
    vs_out.normal = normalize(mat3(T * m_mat) * iv3normal);
}
