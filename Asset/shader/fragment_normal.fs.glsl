#version 410 core
                                                                                  
layout(location = 0) out vec4 fragColor;
/* defer */
layout (location = 1) out vec4 frag_vertex;
layout (location = 2) out vec4 frag_normal;
layout (location = 3) out vec4 frag_ambient;
layout (location = 4) out vec4 frag_diffuse;
layout (location = 5) out vec4 frag_specular;
/* bloom */
layout (location = 6) out vec4 frag_bloom;
                                                                                  
// Color and normal maps
uniform sampler2D tex_color;
uniform sampler2D tex_normal;
uniform sampler2DShadow tex_shadow;
uniform int flag;
/* defer */
uniform vec4 Ka;
uniform vec4 Kd;
uniform vec4 Ks;
                                                        
in VS_OUT
{
    vec3 N;
    vec3 L;
    vec3 L2;
    vec3 V;
    vec4 texcoord_shadow;
    vec2 texcoord;
    vec3 FragPos;
    vec3 TangentLightDir;
    vec3 TangentViewPos;
    vec3 TangentFragPos;
    /* defer */
    vec3 position;
    vec3 normal;
} fs_in;

in float v_depth;
uniform int renderBlur;
uniform int pointLightOpen;
//
//vec4 ambient_albedo = vec4(0.3f, 0.3f, 0.3f, 1.0f);
//vec4 diffuse_albedo = vec4(0.5f, 0.5f, 0.5f, 1.0f);
//vec4 specular_albedo = vec4(1.0f, 1.0f, 1.0f, 1.0f);
vec4 ambient_albedo = vec4(0.1f, 0.1f, 0.1f, 1.0f);
vec4 diffuse_albedo = vec4(0.8f, 0.8f, 0.8f, 1.0f);
vec4 specular_albedo = vec4(0.1f, 0.1f, 0.1f, 1.0f);
float specular_power = 32.0; // shininess
float shadow_factor;

void main(void)
{
    
    // obtain normal from normal map in range [0,1]
    vec3 normal = texture(tex_normal, fs_in.texcoord).rgb;
    // transform normal vector to range [-1,1]
    normal = normalize(normal * 2.0 - 1.0);  // this normal is in tangent space
   
    // get diffuse color
    vec4 color = texture(tex_color, fs_in.texcoord);
    // ambient
    vec4 ambient = ambient_albedo * color;
    // diffuse
    vec3 lightDir = normalize(fs_in.TangentLightDir);
    float diff = max(dot(lightDir, normal), 0.0);
    vec4 diffuse = diffuse_albedo * diff * color;
    
    // specular
    vec3 viewDir = normalize(fs_in.TangentViewPos - fs_in.TangentFragPos);
    vec3 reflectDir = reflect(-lightDir, normal);
    vec3 halfwayDir = normalize(lightDir + viewDir);
    float spec = pow(max(dot(normal, halfwayDir), 0.0), specular_power);

    vec4 specular = specular_albedo * spec;
    
    /* DOF */
    float blur = 0;
    
    float near_distance = 400.0;
    float far_distance = 400.0;
    
    float near_plane = -20.0;
    float far_plane = 1000.0;

    if(v_depth <= near_plane && v_depth >= far_plane)
    {
        blur = 0;
    }
    else if(v_depth > near_plane)
    {
        blur = clamp(v_depth, near_plane, near_plane + near_distance);
        blur = (blur - near_plane) / near_distance;
    }
    else if(v_depth < far_plane)
    {
        blur = clamp(v_depth, far_plane - far_distance, far_plane);
        blur = (far_plane - blur) / far_distance;
    }
    
    /* shadow */
    vec3 N = normalize(fs_in.N);
    vec3 L = normalize(fs_in.L);
    vec3 V = normalize(fs_in.V);
    vec3 R = reflect(-L, N);
    
    vec4 difftex = texture(tex_color, fs_in.texcoord);
    
    vec4 ambient_shadow = difftex * ambient_albedo;
    vec4 diffuse_shadow = difftex * diffuse_albedo * max(dot(N, L), 0.0);
    vec4 specular_shadow = specular_albedo * pow(max(dot(R, V), 0.0), specular_power);
    
    /* point light */
    vec3 L2 = normalize(fs_in.L2);
    vec3 R2 = reflect(-L2, N);
    
    vec4 diffuse2 = difftex * diffuse_albedo * max(dot(N, L2), 0.0);
    vec4 specular2 = specular_albedo * pow(max(dot(R2, V), 0.0), specular_power);
    
    shadow_factor = textureProj(tex_shadow, fs_in.texcoord_shadow);
    if (shadow_factor < 0.2) shadow_factor = 0.2;
    else if (shadow_factor > 1.0) shadow_factor = 1.0;
    
    if (renderBlur == 1)
    {
        if (flag == 1)
        {
            if (pointLightOpen == 1) fragColor = vec4(1.2 * (ambient.xyz + diffuse.xyz + specular.xyz) + (diffuse2.xyz + specular2.xyz), blur);
            else fragColor = vec4(1.2 * (ambient.xyz + diffuse.xyz + specular.xyz), blur);
        }
            
        else
        {
            if (pointLightOpen == 1) fragColor = vec4(1.2 * (ambient_shadow.xyz + (diffuse_shadow.xyz + specular_shadow.xyz) * shadow_factor) + (diffuse2.xyz + specular2.xyz), blur);
            else fragColor = vec4(1.2 * (ambient_shadow.xyz + (diffuse_shadow.xyz + specular_shadow.xyz) * shadow_factor), blur);
        }
            
    }
    else
    {
        if (flag == 1)
        {
            if (pointLightOpen == 1) fragColor = ambient + diffuse + specular + (diffuse2 + specular2);
            else fragColor = ambient + diffuse + specular;
        }
        else
        {
            if (pointLightOpen == 1) fragColor = ambient_shadow + (diffuse_shadow + specular_shadow) * shadow_factor + (diffuse2 + specular2);
            else fragColor = ambient_shadow + (diffuse_shadow + specular_shadow) * shadow_factor;
        }
    }
    /* defer */
    frag_vertex = vec4(fs_in.position, 1.0);
    frag_normal = vec4(fs_in.normal, 1.0);
    frag_ambient = vec4(Ka);
    frag_diffuse = vec4(difftex);
    frag_specular = vec4(Ks);
    /* bloom */
    frag_bloom = vec4(0.0, 0.0, 0.0, 1.0);
}
                                                                                  
