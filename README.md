# Computer graphics final project 

The project is to render a splendid outdoor scene by OpenGL.

### Used render techniques

1. Load models by Assimp
2. Phong shading
3. Directional light shadow
4. Deferred shading
5. Normal mapping
6. Bloom effect
7. Instanced plants rendering
8. Water effect


### Used package
glfw, glad, glw, ImGUI, assimp
