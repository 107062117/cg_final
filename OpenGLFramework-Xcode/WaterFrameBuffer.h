#pragma once


#include <fstream>
#include <tuple>
#include <vector>
#include <iostream>
#include <GLM/mat4x4.hpp>
#include <GLM/gtx/transform.hpp>
#include <GLM/gtc/type_ptr.hpp>
#include "assimp/cimport.h"
#include "assimp/scene.h"
#include "assimp/postprocess.h"
#include "TextureMaterial.h"
#include "Shader.h"

class WaterFrameBuffer {
private:
    int width, height;
	GLuint reflectionFrameBuffer, reflectionDepthBuffer;
    GLuint refractionFrameBuffer, refractionDepthTexture;
	GLuint reflectionTexture, refractionTexture;

	GLuint CreateFrameBuffer();
    GLuint CreateColorTextureAttachment(int width, int height);
	GLuint CreateDepthTextureAttachment(int width, int height);
	GLuint CreateDepthBufferAttachment(int width, int height);
	void BindFrameBuffer(GLuint buffer, int width, int height);

public:
	WaterFrameBuffer(int w, int h);
	~WaterFrameBuffer();

	void Clear();
	void BindReflectionBuffer();
	void BindRefractionBuffer();
	void UnbindBuffer();
    GLuint GetReflectionTexture();
    GLuint GetRefractionTexture();
	GLuint GetRefractionDepthTexture();
};
