#pragma once

#include <fstream>
#include <tuple>
#include <vector>
#include <iostream>
#include <GLM/mat4x4.hpp>
#include <GLM/gtx/transform.hpp>
#include <GLM/gtc/type_ptr.hpp>
#include "assimp/cimport.h"
#include "assimp/scene.h"
#include "assimp/postprocess.h"
#include "TextureMaterial.h"
#include "Shader.h"

class DeferFrameBuffer {
private:
	int frameWidth, frameHeight;

    GLuint geoFrameBuffer, geoDepthBuffer;
    GLuint sceneTexture, vertexTexture, normalTexture, ambientTexture, diffuseTexture, specularTexture;
    
    GLuint CreateFrameBuffer();
    GLuint CreateSceneTextureAttachment(int width, int height);
    GLuint CreateVertexTextureAttachment(int width, int height);
    GLuint CreateNormalTextureAttachment(int width, int height);
    GLuint CreateAmbientTextureAttachment(int width, int height);
    GLuint CreateDiffuseTextureAttachment(int width, int height);
    GLuint CreateSpecularTextureAttachment(int width, int height);
    GLuint CreateDepthBufferAttachment(int width, int height);
    
    void BindFrameBuffer(GLuint buffer, int width, int height);

public:
	DeferFrameBuffer(int, int);
	~DeferFrameBuffer();

	void Clear();
    void BindGeoBuffer();
    void UnbindBuffer();
    GLuint GetSceneTexture();
    GLuint GetVertexTexture();
    GLuint GetNormalTexture();
    GLuint GetAmbientTexture();
    GLuint GetDiffuseTexture();
    GLuint GetSpecularTexture();
};
