#pragma once


#include <fstream>
#include <tuple>
#include <vector>
#include <iostream>
#include <GLM/mat4x4.hpp>
#include <GLM/gtx/transform.hpp>
#include <GLM/gtc/type_ptr.hpp>

#define deg2rad(x) ((x)*((3.1415926f)/(180.0f)))
#define rad2deg(x) ((x)*(180.0f)/(3.1415926f))

class Camera{
private:
    float distance;
    void SetViewMatrix();
public:
	Camera();
	~Camera();
    void InitProjectionMatrix(float fov, float w, float h, float near, float far);
    void InitViewMatrix(glm::vec3 pos, glm::vec3 lookat);
    void Move(float speed);
    void Rotate(float yaw_offset, float pitch_offset);
    float width, height;
    float near, far, fov;
    float pitch, yaw;
    glm::mat4 view_matrix, proj_matirx;
    glm::vec3 position, lookAt;
	
};
