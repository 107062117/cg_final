#include "WaterFrameBuffer.h"

WaterFrameBuffer::WaterFrameBuffer(int w, int h) {
    width = w, height = h;
	reflectionFrameBuffer = CreateFrameBuffer();
    reflectionTexture = CreateColorTextureAttachment(width, height);
	reflectionDepthBuffer = CreateDepthBufferAttachment(width, height);
	UnbindBuffer();

	refractionFrameBuffer = CreateFrameBuffer();
    refractionTexture = CreateColorTextureAttachment(width, height);
	refractionDepthTexture = CreateDepthTextureAttachment(width, height);
	UnbindBuffer();
}

WaterFrameBuffer::~WaterFrameBuffer() {
	glDeleteFramebuffers(1, &reflectionFrameBuffer);
	glDeleteFramebuffers(1, &refractionFrameBuffer);
	glDeleteRenderbuffers(1, &reflectionDepthBuffer);
	glDeleteTextures(1, &refractionDepthTexture);
    glDeleteTextures(1, &reflectionTexture);
    glDeleteTextures(1, &refractionTexture);
}

GLuint WaterFrameBuffer::GetReflectionTexture() {
	return reflectionTexture;
}
GLuint WaterFrameBuffer::GetRefractionTexture() {
	return refractionTexture;
}

GLuint WaterFrameBuffer::GetRefractionDepthTexture() {
	return refractionDepthTexture;
}
void WaterFrameBuffer::Clear() {
    glClearColor(1.0, 1.0, 1.0, 1.0);
	glClear(GL_DEPTH_BUFFER_BIT | GL_COLOR_BUFFER_BIT);
}
void WaterFrameBuffer::BindReflectionBuffer() {
	BindFrameBuffer(reflectionFrameBuffer, width, height);
}
void WaterFrameBuffer::BindRefractionBuffer() {
	BindFrameBuffer(refractionFrameBuffer, width, height);
}

void WaterFrameBuffer::UnbindBuffer() {
	glBindFramebuffer(GL_FRAMEBUFFER, 0);
}
void WaterFrameBuffer::BindFrameBuffer(GLuint buffer, int width, int height) {
	glBindFramebuffer(GL_FRAMEBUFFER, buffer);
    glDrawBuffer(GL_COLOR_ATTACHMENT0);
	glViewport(0, 0, width, height);
}
GLuint WaterFrameBuffer::CreateFrameBuffer() {
	GLuint frameBuffer;
	glGenFramebuffers(1, &frameBuffer);
	glBindFramebuffer(GL_FRAMEBUFFER, frameBuffer);
	glDrawBuffer(GL_COLOR_ATTACHMENT0);

	return frameBuffer;
}

GLuint WaterFrameBuffer::CreateColorTextureAttachment(int width, int height) {
    GLuint texture;
    glGenTextures(1, &texture);
    glBindTexture(GL_TEXTURE_2D, texture);
    glTexImage2D(GL_TEXTURE_2D, 0, GL_RGBA, width, height, 0, GL_RGBA,GL_UNSIGNED_BYTE, NULL );
    glTexParameteri(GL_TEXTURE_2D, GL_TEXTURE_WRAP_S, GL_CLAMP_TO_EDGE );
    glTexParameteri(GL_TEXTURE_2D, GL_TEXTURE_WRAP_T, GL_CLAMP_TO_EDGE );
    glTexParameteri(GL_TEXTURE_2D, GL_TEXTURE_MIN_FILTER, GL_LINEAR );
    glTexParameteri(GL_TEXTURE_2D, GL_TEXTURE_MAG_FILTER, GL_LINEAR );
    glFramebufferTexture2D( GL_FRAMEBUFFER, GL_COLOR_ATTACHMENT0, GL_TEXTURE_2D, texture, 0 );
    return texture;
}

GLuint WaterFrameBuffer::CreateDepthTextureAttachment(int width, int height) {
	GLuint texture;
	glGenTextures(1, &texture);
	glBindTexture(GL_TEXTURE_2D, texture);
	glTexImage2D(GL_TEXTURE_2D, 0, GL_DEPTH_COMPONENT32, width, height, 0, GL_DEPTH_COMPONENT, GL_FLOAT, nullptr);
	glTexParameteri(GL_TEXTURE_2D, GL_TEXTURE_MAG_FILTER, GL_LINEAR);
	glTexParameteri(GL_TEXTURE_2D, GL_TEXTURE_MIN_FILTER, GL_LINEAR);
	glFramebufferTexture(GL_FRAMEBUFFER, GL_DEPTH_ATTACHMENT, texture, 0);

	return texture;
}
GLuint WaterFrameBuffer::CreateDepthBufferAttachment(int width, int height) {
	GLuint depthBuffer;
	glGenRenderbuffers(1, &depthBuffer);
	glBindRenderbuffer(GL_RENDERBUFFER, depthBuffer);
	glRenderbufferStorage(GL_RENDERBUFFER, GL_DEPTH_COMPONENT, width, height);
	glFramebufferRenderbuffer(GL_FRAMEBUFFER, GL_DEPTH_ATTACHMENT, GL_RENDERBUFFER, depthBuffer);

	return depthBuffer;
}
