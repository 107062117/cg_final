#pragma once

#include <fstream>
#include "basic/TextureMaterial.h"
#include "basic/Transformation.h"
#include "basic/SceneManager.h"
#include "Shader.h"
#include "../Camera.h"


class Terrain
{
public:
	Terrain();
	virtual ~Terrain();
	float getHeight(const float x, const float z) const;

private:
	GLuint m_vao;
	GLuint m_dataBuffer;
	int m_dataByteOffset;
	Transformation *m_transform;

private:
	glm::vec3 worldVToHeightMapUV(float x, float z) const;

	void fromMYTD(const std::string &filename);

	int m_width;
	int m_height;
	float m_heightScale;
    Shader* shader;

private:
	GLuint m_indexBuffer;
	glm::mat4 m_worldToHeightUVMat;
	glm::mat4 m_worldToDiffuseUVMat;

	TextureMaterial *m_elevationTex;
	TextureMaterial *m_normalTex;
	TextureMaterial *m_colorTex;
	

	float* m_elevationMap;
	float* m_normalMap;
	float* m_colorMap;

	float m_heightFactor = 50.0;

	// 4 rotation matrix for chunk
	glm::mat4 m_chunkRotMat[4];
    
    struct{
        GLuint m_projMatHandle;
        GLuint m_viewMatHandle;
        GLuint m_modelMatHandle;
        GLuint clip_plane;
        GLuint um4shadow_LOC;
        GLuint m_vToHeightUVMatHandle;
        
        GLuint m_texture0Handle;
        GLuint tex_shadow_LOC;
        GLuint m_normalMapHandle;
        GLuint m_elevationMapHandle;
        GLuint renderBlur;
        GLuint pointLightOpen_LOC;
    }uniform;
    GLenum m_diffuseTexUnit;
    GLenum m_elevationMapTexUnit;
    GLenum m_normalMapTexUnit;

public:
    void render(Camera* camera, glm::vec4 plane, glm::mat4 shadow_sbpv_matrix, GLuint fboDataTexture_shadow, bool blur, bool pointLightOpen);

public:
	void setCameraPosition(const glm::vec3 &pos);
};

