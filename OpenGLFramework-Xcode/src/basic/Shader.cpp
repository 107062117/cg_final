#include "Shader.h"

// version: 160907

Shader::Shader(std::string vetShFile, std::string fraShFile)
{
	/////////////
	// Prepare code buffer
    char** shaderCode;
    shaderCode = this->readProgram((char*)(vetShFile.c_str()));
	GLuint m_vertexShaderId = this->loadShader(shaderCode, GL_VERTEX_SHADER);
	// Set up fragment shader
    shaderCode = this->readProgram((char*)(fraShFile.c_str()));
	GLuint m_fragmentShaderId = this->loadShader(shaderCode, GL_FRAGMENT_SHADER);

	// Create Program
	m_programID = glCreateProgram();

	// Attach shader
	glAttachShader(m_programID, m_vertexShaderId);
	glAttachShader(m_programID, m_fragmentShaderId);

	// Link program
	glLinkProgram(m_programID);

	// It is not required to hande the shader in memory
	glDeleteShader(m_vertexShaderId);
	glDeleteShader(m_fragmentShaderId);

	// Delete shader code
	delete[] shaderCode;
}
Shader::Shader(char *vetShFile, char *geoShFile, char *fraShFile){
	/////////////
	// Set up vertex shader
    char** shaderCode;
    shaderCode = this->readProgram(vetShFile);
	GLuint m_vertexShaderId = this->loadShader(shaderCode, GL_VERTEX_SHADER);
	// Set up geometry shader
    shaderCode = this->readProgram(geoShFile);
	GLuint m_geometryShaderId = this->loadShader(shaderCode, GL_GEOMETRY_SHADER);
	// Set up fragment shader
    shaderCode = this->readProgram(fraShFile);
	GLuint m_fragmentShaderId = this->loadShader(shaderCode, GL_FRAGMENT_SHADER);

	// Create Program
	m_programID = glCreateProgram();

	// Attach shader
	glAttachShader(m_programID, m_vertexShaderId);
	glAttachShader(m_programID, m_geometryShaderId);
	glAttachShader(m_programID, m_fragmentShaderId);

	// Link program
	glLinkProgram(m_programID);

	// It is not required to hande the shader in memory
	glDeleteShader(m_vertexShaderId);
	glDeleteShader(m_fragmentShaderId);

	// Delete shader code
	delete[] shaderCode;
}

Shader::~Shader(void)
{
	glDeleteProgram(m_programID);
}


char** Shader::readProgram(char* file)
{
    FILE* fp = fopen(file, "rb");
    fseek(fp, 0, SEEK_END);
    long sz = ftell(fp);
    fseek(fp, 0, SEEK_SET);
    char *src = new char[sz + 1];
    fread(src, sizeof(char), sz, fp);
    src[sz] = '\0';
    char **srcp = new char*[1];
    srcp[0] = src;
    return srcp;
}


int Shader::loadShader(char **shaderCode, GLenum shaderType){
	int shaderID ;
	shaderID = glCreateShader(shaderType) ;
	glShaderSource(shaderID, 1, shaderCode, NULL) ;
	glCompileShader(shaderID) ;
	char compileError[1000] = "";
	glGetShaderInfoLog(shaderID, 1000, NULL, compileError) ;
	if(strcmp(compileError, "")){
		cout << compileError << endl ;
		system("pause") ;
		exit(0) ;
	}
	cout << "shader compiling complete !!" << endl ;
	return shaderID ;	
}

void Shader::useShader(){	
	glUseProgram(m_programID);
}
void Shader::disableShader(){
	glUseProgram(0);
}
GLuint Shader::getProgramID(){
	return m_programID ;
}
