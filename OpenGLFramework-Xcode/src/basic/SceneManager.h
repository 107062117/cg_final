#pragma once

#include <GLM/mat4x4.hpp>
#include <GLM/gtx/transform.hpp>
#include <GLM/gtc/type_ptr.hpp>


#include <glad/glad.h>

// Singleton

class SceneManager
{
private:
	SceneManager(){}
	

public:	
	
	virtual ~SceneManager(){}

	static SceneManager *Instance(){
		static SceneManager *m_instance = nullptr;
		if (m_instance == nullptr){
			m_instance = new SceneManager();
		}
		return m_instance;
	}

	GLint m_vertexHandle;
	GLint m_normalHandle;
	GLint m_uvHandle;

	GLuint m_projMatHandle;
	GLuint m_viewMatHandle;
	GLuint m_modelMatHandle;
    
    GLuint um4shadow_LOC;
    
    GLint tex_shadow_LOC;
    
    GLuint clip_plane;

	GLuint m_texture0Handle;
    

	GLuint m_vToHeightUVMatHandle;
	GLuint m_elevationMapHandle;
	GLuint m_normalMapHandle;	
	
	GLenum m_elevationMapTexUnit;
	GLenum m_normalMapTexUnit;
	GLenum m_diffuseTexUnit;
    
    
};

