#pragma once


#include <fstream>
#include <tuple>
#include <vector>
#include <iostream>
#include <GLM/mat4x4.hpp>
#include <GLM/gtx/transform.hpp>
#include <GLM/gtc/type_ptr.hpp>
#include "assimp/cimport.h"
#include "assimp/scene.h"
#include "assimp/postprocess.h"
#include "TextureMaterial.h"
#include "Shader.h"
#include "WaterFrameBuffer.h"
#include "Camera.h"

class Water{
private:
    struct Shape
    {
        GLuint vao;
        GLuint vbo_position;
        GLuint vbo_normal;
        GLuint vbo_texcoord;
        GLuint ibo;
        int drawCount;
        int materialID;
    };

    struct Material
    {
        TextureMaterial *diff_tex;
    };

    struct Model
    {
        glm::vec3 position = glm::vec3(512, 107, 512);
        glm::vec3 scale = glm::vec3(1, 1.0, 1);
        glm::vec3 rotation = glm::vec3(0, 0, 0);
        std::vector<Shape> shapes;
        std::vector<Material> materials;
    };
    struct
    {
        GLuint m_mat;
        GLuint v_mat;
        GLuint p_mat;
        GLuint clip_plane;
        GLuint reflectionSampler;
        GLuint refractionSampler;
        GLuint dudvSampler;
        GLuint normalSampler;
        GLuint depthSampler;
        GLuint near_clip;
        GLuint far_clip;
        GLuint light_color;
        GLuint cam_pos;
        GLuint move;
        GLuint tile;
        GLuint distorsion;
        GLuint specular;
        GLuint renderBlur;
    } uniform;
    Model model;
	Shader* shader;
	float moveFactor;
    TextureMaterial* dudv_tex;
    TextureMaterial* normal_tex;
    glm::vec3 lightColor = glm::vec3(1.0, 1.0, 1.0);

public:
	Water();
	~Water();

	float textureTiling;
	float distorsionStrength;
	float moveSpeed;
	float specularPower;

    GLuint GetDUDV();
    GLuint GetNormal();
	glm::mat4 GetTransform();
	float GetMoveFactor();
    float GetHeight();
    Model LoadModels(std::string filePath);
    TextureMaterial* LoadTexture(std::string texturePath);
    void render(WaterFrameBuffer* waterFrameBuffer, Camera *camera, bool blur);
};
