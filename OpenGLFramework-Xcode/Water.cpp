#include "Water.h"
#include<vector>


Water::Water() {
	textureTiling = 1;
	moveFactor = 0.0f;
	moveSpeed = 1.0f/1000.0f;
	distorsionStrength = 0.04f;
	specularPower = 20.0f;
    
    this->model = LoadModels("models/waterPlane.obj");
    this->dudv_tex = LoadTexture("models/textures/dudv.jpg");
    this->normal_tex = LoadTexture("models/textures/normal.jpg");
    
    // setup program
    this->shader = new Shader("shader/vertex_water.vs.glsl", "shader/fragment_water.fs.glsl");
    this->shader->useShader();
    GLuint programId = this->shader->getProgramID();
    this->uniform.m_mat = glGetUniformLocation(programId, "gWorld");
    this->uniform.v_mat = glGetUniformLocation(programId, "gCamera");
    this->uniform.p_mat = glGetUniformLocation(programId, "gProj");
        
    this->uniform.reflectionSampler = glGetUniformLocation(programId, "reflectionSampler");
    this->uniform.refractionSampler = glGetUniformLocation(programId, "refractionSampler");
    this->uniform.dudvSampler = glGetUniformLocation(programId, "dudvSampler");
    this->uniform.normalSampler = glGetUniformLocation(programId, "normalSampler");
    this->uniform.depthSampler = glGetUniformLocation(programId, "depthSampler");
    
    this->uniform.near_clip = glGetUniformLocation(programId, "near");
    this->uniform.far_clip = glGetUniformLocation(programId, "far");
    this->uniform.light_color = glGetUniformLocation(programId, "lightColor");
    this->uniform.cam_pos = glGetUniformLocation(programId, "cameraPosition");
    this->uniform.move = glGetUniformLocation(programId, "moveFactor");
    this->uniform.tile = glGetUniformLocation(programId, "textureTiling");
    this->uniform.distorsion = glGetUniformLocation(programId, "distorsionStrength");
    this->uniform.specular = glGetUniformLocation(programId, "specularPower");
    
    this->uniform.renderBlur = glGetUniformLocation(programId, "renderBlur");
    
}

Water::Model Water::LoadModels(std::string filePath)
{
    Model model;
    const aiScene *scene = aiImportFile(filePath.c_str(), aiProcessPreset_TargetRealtime_MaxQuality);
    if(!scene) std::cout << "Fail load scene " + filePath+"\n";

    std::vector<Shape> shapes;
    for (unsigned int i = 0; i < scene->mNumMeshes; ++i)
    {
        aiMesh *mesh = scene->mMeshes[i];
        Shape shape;
        glGenVertexArrays(1, &shape.vao);
        glBindVertexArray(shape.vao);

        std::vector<float> vertices, texcoords, normals;
        // create 3 vbos to hold data
        for (unsigned int v = 0; v < mesh->mNumVertices; ++v)
        {
            vertices.push_back(mesh->mVertices[v][0]);
            vertices.push_back(mesh->mVertices[v][1]);
            vertices.push_back(mesh->mVertices[v][2]);
            texcoords.push_back(mesh->mTextureCoords[0][v][0]);
            texcoords.push_back(1-mesh->mTextureCoords[0][v][1]);
            normals.push_back(mesh->mNormals[v][0]);
            normals.push_back(mesh->mNormals[v][1]);
            normals.push_back(mesh->mNormals[v][2]);
        }
        std::vector<int> indices;
        // create 1 ibo to hold data
        for (unsigned int f = 0; f < mesh->mNumFaces; ++f)
        {
            indices.push_back(mesh->mFaces[f].mIndices[0]);
            indices.push_back(mesh->mFaces[f].mIndices[1]);
            indices.push_back(mesh->mFaces[f].mIndices[2]);
        }

        // glVertexAttribPointer / glEnableVertexArray calls...
        glGenBuffers(1, &shape.vbo_position);
        glBindBuffer(GL_ARRAY_BUFFER, shape.vbo_position);
        glBufferData(GL_ARRAY_BUFFER, vertices.size() * sizeof(float), &vertices[0], GL_STATIC_DRAW);
        glVertexAttribPointer(0, 3, GL_FLOAT, GL_FALSE, 0, 0);
        glEnableVertexAttribArray(0);

        glGenBuffers(1, &shape.vbo_normal);
        glBindBuffer(GL_ARRAY_BUFFER, shape.vbo_normal);
        glBufferData(GL_ARRAY_BUFFER, normals.size() * sizeof(float), &normals[0], GL_STATIC_DRAW);
        glVertexAttribPointer(1, 3, GL_FLOAT, GL_FALSE, 0, 0);
        glEnableVertexAttribArray(1);

        glGenBuffers(1, &shape.vbo_texcoord);
        glBindBuffer(GL_ARRAY_BUFFER, shape.vbo_texcoord);
        glBufferData(GL_ARRAY_BUFFER, texcoords.size() * sizeof(float), &texcoords[0], GL_STATIC_DRAW);
        glVertexAttribPointer(2, 2, GL_FLOAT, GL_FALSE, 0, 0);
        glEnableVertexAttribArray(2);

        glGenBuffers(1, &shape.ibo);
        glBindBuffer(GL_ELEMENT_ARRAY_BUFFER, shape.ibo);
        glBufferData(GL_ELEMENT_ARRAY_BUFFER, indices.size() * sizeof(float), &indices[0], GL_STATIC_DRAW);

        shape.materialID = mesh->mMaterialIndex;
        shape.drawCount = mesh->mNumFaces * 3;
        // save shape...
        shapes.push_back(shape);
    }
    aiReleaseImport(scene);
    model.shapes = shapes;
    return model;
}

TextureMaterial* Water::LoadTexture(std::string texturePath)
{
    TextureMaterial *tex = new TextureMaterial(std::string(texturePath.c_str()));
    tex->setTextureUnit(GL_TEXTURE3);
    std::cout << "Load tex "+std::string(texturePath.c_str())+"\n";
    return tex;
}

Water::~Water() {
}

GLuint Water::GetDUDV() {
	return dudv_tex->handle();
}

GLuint Water::GetNormal() {
	return normal_tex->handle();
}

float Water::GetMoveFactor() {
	return moveFactor;
}

void Water::render(WaterFrameBuffer* waterFrameBuffer, Camera *camera, bool blur) {
    glEnable(GL_BLEND);
    glBlendFunc(GL_SRC_ALPHA, GL_ONE_MINUS_SRC_ALPHA);
    this->shader->useShader();
    moveFactor += moveSpeed;
    if (moveFactor == 1.0)
        moveFactor = 0.0;
    
    glUniform1i(this->uniform.reflectionSampler, 3);
    glUniform1i(this->uniform.refractionSampler, 4);
    glUniform1i(this->uniform.dudvSampler, 5);
    glUniform1i(this->uniform.normalSampler, 6);
    glUniform1i(this->uniform.depthSampler, 7);
    
    glBindVertexArray(this->model.shapes[0].vao);
    glBindBuffer(GL_ELEMENT_ARRAY_BUFFER, this->model.shapes[0].ibo);
    
    glActiveTexture(GL_TEXTURE3);
    glBindTexture(GL_TEXTURE_2D, waterFrameBuffer->GetReflectionTexture());
    glActiveTexture(GL_TEXTURE4);
    glBindTexture(GL_TEXTURE_2D, waterFrameBuffer->GetRefractionTexture());
    glActiveTexture(GL_TEXTURE5);
    glBindTexture(GL_TEXTURE_2D, GetDUDV());
    glActiveTexture(GL_TEXTURE6);
    glBindTexture(GL_TEXTURE_2D, GetNormal());
    glActiveTexture(GL_TEXTURE7);
    glBindTexture(GL_TEXTURE_2D, waterFrameBuffer->GetRefractionDepthTexture());
    
    
    glm::mat4 proj_matrix = camera->proj_matirx;
    glm::mat4 view_matrix = camera->view_matrix;

    glm::mat4 T, S, R;
    T = glm::translate(glm::mat4(1.0), this->model.position);
    S = glm::scale(glm::mat4(1.0), this->model.scale);
    R = glm::mat4(1.0);
    glm::mat4 model_matrix = T * R * S;
    
    glUniform1f(this->uniform.near_clip, camera->near);
    glUniform1f(this->uniform.far_clip, camera->far);
    glUniform3fv(this->uniform.light_color, 1, &lightColor[0]);
    glUniform3fv(this->uniform.cam_pos, 1, &camera->position[0]);
    glUniform1f(this->uniform.move, moveFactor);
    glUniform1f(this->uniform.tile, textureTiling);
    glUniform1f(this->uniform.distorsion, distorsionStrength);
    glUniform1f(this->uniform.specular, specularPower);
    glUniformMatrix4fv(this->uniform.m_mat, 1, GL_FALSE, &model_matrix[0][0]);
    glUniformMatrix4fv(this->uniform.v_mat, 1, GL_FALSE, &view_matrix[0][0]);
    glUniformMatrix4fv(this->uniform.p_mat, 1, GL_FALSE, &proj_matrix[0][0]);
    
    glUniform1f(this->uniform.renderBlur, blur);

    glDrawElements(GL_TRIANGLES, this->model.shapes[0].drawCount, GL_UNSIGNED_INT, 0);

    glDisable(GL_BLEND);
}
float Water::GetHeight()
{
    return model.position.y;
}
