#include "DeferFrameBuffer.h"

DeferFrameBuffer::DeferFrameBuffer(int w, int h) {
    frameWidth = w;
    frameHeight = h;
    geoFrameBuffer = CreateFrameBuffer();
    sceneTexture = CreateSceneTextureAttachment(frameWidth, frameHeight);
    vertexTexture = CreateVertexTextureAttachment(frameWidth, frameHeight);
    normalTexture = CreateNormalTextureAttachment(frameWidth, frameHeight);
    ambientTexture = CreateAmbientTextureAttachment(frameWidth, frameHeight);
    diffuseTexture = CreateDiffuseTextureAttachment(frameWidth, frameHeight);
    specularTexture = CreateSpecularTextureAttachment(frameWidth, frameHeight);
    geoDepthBuffer = CreateDepthBufferAttachment(frameWidth, frameHeight);
    UnbindBuffer();
}

DeferFrameBuffer::~DeferFrameBuffer() {
    glDeleteFramebuffers(1, &geoFrameBuffer);
    glDeleteRenderbuffers(1, &geoDepthBuffer);
    glDeleteTextures(1, &sceneTexture);
    glDeleteTextures(1, &vertexTexture);
    glDeleteTextures(1, &normalTexture);
    glDeleteTextures(1, &ambientTexture);
    glDeleteTextures(1, &diffuseTexture);
    glDeleteTextures(1, &specularTexture);
}

GLuint DeferFrameBuffer::GetSceneTexture() {
    return sceneTexture;
}
GLuint DeferFrameBuffer::GetVertexTexture() {
    return vertexTexture;
}
GLuint DeferFrameBuffer::GetNormalTexture() {
    return normalTexture;
}
GLuint DeferFrameBuffer::GetAmbientTexture() {
    return ambientTexture;
}
GLuint DeferFrameBuffer::GetDiffuseTexture() {
    return diffuseTexture;
}
GLuint DeferFrameBuffer::GetSpecularTexture() {
    return specularTexture;
}

void DeferFrameBuffer::Clear() {
    glDrawBuffer(GL_COLOR_ATTACHMENT0);
    glClear(GL_DEPTH_BUFFER_BIT | GL_COLOR_BUFFER_BIT);
    glDrawBuffer(GL_COLOR_ATTACHMENT1);
    glClear(GL_DEPTH_BUFFER_BIT | GL_COLOR_BUFFER_BIT);
    glDrawBuffer(GL_COLOR_ATTACHMENT2);
    glClear(GL_DEPTH_BUFFER_BIT | GL_COLOR_BUFFER_BIT);
    glDrawBuffer(GL_COLOR_ATTACHMENT3);
    glClear(GL_DEPTH_BUFFER_BIT | GL_COLOR_BUFFER_BIT);
    glDrawBuffer(GL_COLOR_ATTACHMENT4);
    glClear(GL_DEPTH_BUFFER_BIT | GL_COLOR_BUFFER_BIT);
    glDrawBuffer(GL_COLOR_ATTACHMENT5);
    glClear(GL_DEPTH_BUFFER_BIT | GL_COLOR_BUFFER_BIT);
//    glClearColor(1.0, 1.0, 1.0, 1.0);
}
void DeferFrameBuffer::BindGeoBuffer() {
    BindFrameBuffer(geoFrameBuffer, frameWidth, frameHeight);
}

void DeferFrameBuffer::UnbindBuffer() {
    glBindFramebuffer(GL_FRAMEBUFFER, 0);
}
void DeferFrameBuffer::BindFrameBuffer(GLuint buffer, int width, int height) {
    glBindFramebuffer(GL_FRAMEBUFFER, buffer);
    const GLenum draw_buffers[] = { GL_COLOR_ATTACHMENT0, GL_COLOR_ATTACHMENT1, GL_COLOR_ATTACHMENT2, GL_COLOR_ATTACHMENT3};
    glDrawBuffers(4, draw_buffers);
    glViewport(0, 0, width, height);
}

GLuint DeferFrameBuffer::CreateFrameBuffer() {
    GLuint frameBuffer;
    glGenFramebuffers(1, &frameBuffer);
    glBindFramebuffer(GL_FRAMEBUFFER, frameBuffer);
//    glDrawBuffer(GL_COLOR_ATTACHMENT0);
    const GLenum draw_buffers[] = { GL_COLOR_ATTACHMENT0, GL_COLOR_ATTACHMENT1, GL_COLOR_ATTACHMENT2, GL_COLOR_ATTACHMENT3};
    glDrawBuffers(4, draw_buffers);

    return frameBuffer;
}

GLuint DeferFrameBuffer::CreateSceneTextureAttachment(int width, int height) {
    GLuint texture;
    glGenTextures(1, &texture);
    glBindTexture(GL_TEXTURE_2D, texture);
    glTexImage2D(GL_TEXTURE_2D, 0, GL_RGBA, width, height, 0, GL_RGBA,GL_UNSIGNED_BYTE, NULL );
    glTexParameteri(GL_TEXTURE_2D, GL_TEXTURE_WRAP_S, GL_CLAMP_TO_EDGE );
    glTexParameteri(GL_TEXTURE_2D, GL_TEXTURE_WRAP_T, GL_CLAMP_TO_EDGE );
    glTexParameteri(GL_TEXTURE_2D, GL_TEXTURE_MIN_FILTER, GL_LINEAR );
    glTexParameteri(GL_TEXTURE_2D, GL_TEXTURE_MAG_FILTER, GL_LINEAR );
    glFramebufferTexture2D( GL_FRAMEBUFFER, GL_COLOR_ATTACHMENT0, GL_TEXTURE_2D, texture, 0 );
    return texture;
}
GLuint DeferFrameBuffer::CreateVertexTextureAttachment(int width, int height) {
    GLuint texture;
    glGenTextures(1, &texture);
    glBindTexture(GL_TEXTURE_2D, texture);
    glTexImage2D(GL_TEXTURE_2D, 0, GL_RGBA32F, width, height, 0, GL_RGBA, GL_UNSIGNED_BYTE, NULL);
    glTexParameteri(GL_TEXTURE_2D, GL_TEXTURE_MIN_FILTER, GL_NEAREST);
    glTexParameteri(GL_TEXTURE_2D, GL_TEXTURE_MAG_FILTER, GL_NEAREST);
    glFramebufferTexture2D(GL_FRAMEBUFFER, GL_COLOR_ATTACHMENT1, GL_TEXTURE_2D, texture, 0);
    return texture;
}

GLuint DeferFrameBuffer::CreateNormalTextureAttachment(int width, int height) {
    GLuint texture;
    glGenTextures(1, &texture);
    glBindTexture(GL_TEXTURE_2D, texture);
    glTexImage2D(GL_TEXTURE_2D, 0, GL_RGBA32F, width, height, 0, GL_RGBA, GL_UNSIGNED_BYTE, NULL);
    glTexParameteri(GL_TEXTURE_2D, GL_TEXTURE_MIN_FILTER, GL_NEAREST);
    glTexParameteri(GL_TEXTURE_2D, GL_TEXTURE_MAG_FILTER, GL_NEAREST);
    glFramebufferTexture2D(GL_FRAMEBUFFER, GL_COLOR_ATTACHMENT2, GL_TEXTURE_2D, texture, 0);
    return texture;
}

GLuint DeferFrameBuffer::CreateAmbientTextureAttachment(int width, int height) {
    GLuint texture;
    glGenTextures(1, &texture);
    glBindTexture(GL_TEXTURE_2D, texture);
    glTexImage2D(GL_TEXTURE_2D, 0, GL_RGBA8, width, height, 0, GL_RGBA, GL_UNSIGNED_BYTE, NULL);
    glTexParameteri(GL_TEXTURE_2D, GL_TEXTURE_MIN_FILTER, GL_NEAREST);
    glTexParameteri(GL_TEXTURE_2D, GL_TEXTURE_MAG_FILTER, GL_NEAREST);
    glFramebufferTexture2D(GL_FRAMEBUFFER, GL_COLOR_ATTACHMENT3, GL_TEXTURE_2D, texture, 0);
    return texture;
}

GLuint DeferFrameBuffer::CreateDiffuseTextureAttachment(int width, int height) {
    GLuint texture;
    glGenTextures(1, &texture);
    glBindTexture(GL_TEXTURE_2D, texture);
    glTexImage2D(GL_TEXTURE_2D, 0, GL_RGBA8, width, height, 0, GL_RGBA, GL_UNSIGNED_BYTE, NULL);
    glTexParameteri(GL_TEXTURE_2D, GL_TEXTURE_MIN_FILTER, GL_NEAREST);
    glTexParameteri(GL_TEXTURE_2D, GL_TEXTURE_MAG_FILTER, GL_NEAREST);
    glFramebufferTexture2D(GL_FRAMEBUFFER, GL_COLOR_ATTACHMENT4, GL_TEXTURE_2D, texture, 0);
    return texture;
}

GLuint DeferFrameBuffer::CreateSpecularTextureAttachment(int width, int height) {
    GLuint texture;
    glGenTextures(1, &texture);
    glBindTexture(GL_TEXTURE_2D, texture);
    glTexImage2D(GL_TEXTURE_2D, 0, GL_RGBA8, width, height, 0, GL_RGBA, GL_UNSIGNED_BYTE, NULL);
    glTexParameteri(GL_TEXTURE_2D, GL_TEXTURE_MIN_FILTER, GL_NEAREST);
    glTexParameteri(GL_TEXTURE_2D, GL_TEXTURE_MAG_FILTER, GL_NEAREST);
    glFramebufferTexture2D(GL_FRAMEBUFFER, GL_COLOR_ATTACHMENT5, GL_TEXTURE_2D, texture, 0);
    return texture;
}

GLuint DeferFrameBuffer::CreateDepthBufferAttachment(int width, int height) {
    GLuint depthBuffer;
    glGenRenderbuffers(1, &depthBuffer);
    glBindRenderbuffer(GL_RENDERBUFFER, depthBuffer);
    glRenderbufferStorage(GL_RENDERBUFFER, GL_DEPTH_COMPONENT32, width, height);
    glFramebufferRenderbuffer(GL_FRAMEBUFFER, GL_DEPTH_ATTACHMENT, GL_RENDERBUFFER, depthBuffer);

    return depthBuffer;
}
