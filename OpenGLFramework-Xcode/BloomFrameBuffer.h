#pragma once

#include <fstream>
#include <tuple>
#include <vector>
#include <iostream>
#include <GLM/mat4x4.hpp>
#include <GLM/gtx/transform.hpp>
#include <GLM/gtc/type_ptr.hpp>
#include "assimp/cimport.h"
#include "assimp/scene.h"
#include "assimp/postprocess.h"
#include "TextureMaterial.h"
#include "Shader.h"

class BloomFrameBuffer {
private:
	int frameWidth, frameHeight;

    GLuint bloomFrameBuffer, bloomDepthBuffer;
    GLuint sceneTexture, ballMaskTexture, blurTexture;
    
    GLuint CreateFrameBuffer();
    GLuint CreateSceneTextureAttachment(int width, int height);
    GLuint CreateBallMaskTextureAttachment(int width, int height);
    GLuint CreateBlurTextureAttachment(int width, int height);
    GLuint CreateDepthBufferAttachment(int width, int height);
    
    void BindFrameBuffer(GLuint buffer, int width, int height);


public:
	BloomFrameBuffer(int, int);
	~BloomFrameBuffer();

	void Clear();
    void BindblurBuffer();
    void UnbindBuffer();
    GLuint GetSceneTexture();
    GLuint GetBallMaskTexture();
    GLuint GetBlurTexture();
};
