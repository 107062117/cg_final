#include "src/Terrain.h"
#include <GLFW/glfw3.h>
#include "SceneSetting.h"
#include "imgui.h"
#include "imgui_impl_glfw.h"
#include "imgui_impl_opengl3.h"
#include "Shader.h"
#include "Water.h"
#include "Camera.h"

using namespace glm;
using namespace std;

#define ORIGIN 0
#define DOF 1
#define DEFER_POS 2
#define DEFER_NORMAL 3
#define DEFER_AMBIENT 4
#define DEFER_DIFFUSE 5
#define DEFER_SPECULAR 6
#define BLOOM 7
int effectMode = ORIGIN;

int FRAME_WIDTH = 1024;
int FRAME_HEIGHT = 768;

void mouseButtonCallback(GLFWwindow* window, int button, int action, int mods);
void mouseScrollCallback(GLFWwindow *window, double xoffset, double yoffset);
void cursorPosCallback(GLFWwindow* window, double x, double y);
void keyCallback(GLFWwindow* window, int key, int scancode, int action, int mods);

void initializeGL();
void resizeGL(GLFWwindow *window, int w, int h);
void paintGL();
void updateState();
void adjustCameraPositionWithTerrain();
void updateAirplane(const glm::mat4 &viewMatrix);
void initScene();
void My_GUI();

bool m_leftButtonPressed = false;
bool m_rightButtonPressed = false;
double cursorPos[2];

/* camera info */
vec3 light_pos;
float cam_fov = 60.0f;
float cam_near_plane = 0.1f;
float cam_far_plane = 1000.0f;
float m_speed;
bool my_gui_active = true;
glm::vec3 in_eye;
glm::vec3 in_lookAt;
bool isPress = false;

void vsyncEnabled(GLFWwindow *window);
void vsyncDisabled(GLFWwindow *window);

PlantManager *m_plantManager = nullptr;
Terrain *m_terrain = nullptr;
ObjManager *m_objManager = nullptr;
AirPlaneManager *m_airplaneManager = nullptr;
Water *m_waterManager = nullptr;
WaterFrameBuffer *m_waterFB = nullptr;
WindowManager *m_windowManager = nullptr;
Camera *m_camera = nullptr;
SphereManager *m_sphere = nullptr;
/* water*/
Camera *w_camera = nullptr;
/* defer */
DeferFrameBuffer *m_deferFB = nullptr;
BloomFrameBuffer *m_bloomFB = nullptr;

// the airplane's transformation has been handled
glm::vec3 m_airplanePosition;
glm::mat4 m_airplaneRotMat;

/* ***** shadow ***** */
GLint um4mvp_shadow_LOC;
GLint um4m_LOC;
GLuint tree_um4vp_shadow_LOC;
GLint tree_um4m_LOC;
GLint tree_tex;

Shader *shadow_program = nullptr;
Shader *tree_shadow_program = nullptr;
GLuint fbo_shadow;
GLuint fboDataTexture_shadow;

/* window */
GLuint window_vao;
GLuint window_vertex_buffer;
GLuint fbo_window;
GLuint depthrbo_window;
GLuint fboDataTexture_window;
static const GLfloat window_vertex[] = {
    //vec2 position vec2 texture_coord
    1.0f, -1.0f, 1.0f, 0.0f,
   -1.0f, -1.0f, 0.0f, 0.0f,
   -1.0f,  1.0f, 0.0f, 1.0f,
    1.0f, 1.0f, 1.0f, 1.0f
};

/* DOF */
Shader *DOF_program = nullptr;
GLuint fbo_DOF;
GLuint depthrbo_DOF;
GLuint fboDataTexture_DOF;
GLuint tex_DOF_LOC;
/* blur */
Shader *blur_program = nullptr;
GLuint fbo_blur;
GLuint depthrbo_blur;
GLuint fboDataTexture_blur;
GLuint tex_blur_LOC;
/* bloom */
Shader *bloom_program = nullptr;
GLuint fbo_bloom;
GLuint depthrbo_bloom;
GLuint fboDataTexture_bloom;
GLuint tex_bloom_scene_LOC;
GLuint tex_bloom_blur_LOC;
bool pointLightOpen = false;

int main(){
    glfwInit();
    glfwWindowHint(GLFW_CONTEXT_VERSION_MAJOR, 3);
    glfwWindowHint(GLFW_CONTEXT_VERSION_MINOR, 3);
    glfwWindowHint(GLFW_OPENGL_PROFILE, GLFW_OPENGL_CORE_PROFILE);
    
    //modify
#ifdef __APPLE__
    glfwWindowHint(GLFW_OPENGL_FORWARD_COMPAT, GL_TRUE); // fix compilation on OS X
#endif
    glfwWindowHint(GLFW_COCOA_RETINA_FRAMEBUFFER, GLFW_FALSE);
    //
    GLFWwindow *window = glfwCreateWindow(FRAME_WIDTH, FRAME_HEIGHT, "rendering", nullptr, nullptr);
    if (window == nullptr){
        std::cout << "failed to create GLFW window\n";
        glfwTerminate();
        return -1;
    }
    glfwSetWindowPos(window, 100, 100);
    glfwMakeContextCurrent(window);
    
    IMGUI_CHECKVERSION();
    ImGui::CreateContext();
    ImGuiIO& io = ImGui::GetIO(); (void)io;
    ImGui::StyleColorsDark();
    ImGui_ImplGlfw_InitForOpenGL(window, true);
    ImGui_ImplOpenGL3_Init("#version 410");

    // load OpenGL function pointer
    if (!gladLoadGLLoader((GLADloadproc)glfwGetProcAddress))
    {
        std::cout << "Failed to initialize GLAD" << std::endl;
        return -1;
    }

    glfwSetKeyCallback(window, keyCallback);
    glfwSetScrollCallback(window, mouseScrollCallback);
    glfwSetMouseButtonCallback(window, mouseButtonCallback);
    glfwSetCursorPosCallback(window, cursorPosCallback);
    glfwSetFramebufferSizeCallback(window, resizeGL);

    initializeGL();

    vsyncEnabled(window);

    // be careful v-sync issue
    glfwSwapInterval(0);
    vsyncDisabled(window);

    glfwTerminate();
    return 0;
}
void vsyncDisabled(GLFWwindow *window){
    float periodForEvent = 1.0 / 60.0;
    float accumulatedEventPeriod = 0.0;
    double previousTime = glfwGetTime();
    double previousTimeForFPS = glfwGetTime();
    int frameCount = 0;
    while (!glfwWindowShouldClose(window)){
        // measure speed
        double currentTime = glfwGetTime();
        float deltaTime = currentTime - previousTime;
        frameCount = frameCount + 1;
        if (currentTime - previousTimeForFPS >= 1.0){
            std::cout << "\rFPS: " << frameCount;
            frameCount = 0;
            previousTimeForFPS = currentTime;
        }
        previousTime = currentTime;

        // game loop
        accumulatedEventPeriod = accumulatedEventPeriod + deltaTime;
        if (accumulatedEventPeriod > periodForEvent){
            updateState();
            accumulatedEventPeriod = accumulatedEventPeriod - periodForEvent;
        }
        paintGL();
        glfwSwapBuffers(window);
        glfwPollEvents();
    }
}
void vsyncEnabled(GLFWwindow *window){
    double previousTimeForFPS = glfwGetTime();
    int frameCount = 0;
    while (!glfwWindowShouldClose(window)){
        // measure speed
        double currentTime = glfwGetTime();
        frameCount = frameCount + 1;
        if (currentTime - previousTimeForFPS >= 1.0){
            std::cout << "\rFPS: " << frameCount;
            frameCount = 0;
            previousTimeForFPS = currentTime;
        }
        
        updateState();
        paintGL();

        glfwSwapBuffers(window);
        glfwPollEvents();
    }
}

void initShader()
{
    /* shadow */
    shadow_program = new Shader("shader/vertex.vs_shadow.glsl", "shader/fragment.fs_shadow.glsl");
    um4mvp_shadow_LOC = glGetUniformLocation(shadow_program->getProgramID(), "um4mvp_shadow");
    um4m_LOC = glGetUniformLocation(shadow_program->getProgramID(), "um4m");
    
    /* tree shadow */
    tree_shadow_program = new Shader("shader/vertex.vs_treeShadow.glsl", "shader/fragment.fs_treeShadow.glsl");
    tree_um4vp_shadow_LOC = glGetUniformLocation(tree_shadow_program->getProgramID(), "um4vp_shadow");
    tree_um4m_LOC = glGetUniformLocation(tree_shadow_program->getProgramID(), "um4m");
    tree_tex = glGetUniformLocation(tree_shadow_program->getProgramID(), "tex");
    
    /* DOF */
    DOF_program = new Shader("shader/vertex_window.vs.glsl", "shader/fragment.fs_DOF.glsl");
    tex_DOF_LOC = glGetUniformLocation(DOF_program->getProgramID(), "tex");
    glUniform1i(tex_DOF_LOC, 6);
    
    /* Blur */
    blur_program = new Shader("shader/vertex_window.vs.glsl", "shader/fragmentBlur.fs.glsl");
    tex_blur_LOC = glGetUniformLocation(blur_program->getProgramID(), "tex");
    glUniform1i(tex_blur_LOC, 6);
    
    /* Bloom */
    bloom_program = new Shader("shader/vertex_window.vs.glsl", "shader/fragment_bloom_mix.fs.glsl");
    tex_bloom_scene_LOC = glGetUniformLocation(bloom_program->getProgramID(), "sceneTex");
    tex_bloom_blur_LOC = glGetUniformLocation(bloom_program->getProgramID(), "blurTex");
    glUniform1i(tex_bloom_scene_LOC, 5);
    glUniform1i(tex_bloom_blur_LOC, 6);
}

void initWindow()
{
    glGenVertexArrays(1, &window_vao);
    glBindVertexArray(window_vao);

    glGenBuffers(1, &window_vertex_buffer);
    glBindBuffer(GL_ARRAY_BUFFER, window_vertex_buffer);
    glBufferData(GL_ARRAY_BUFFER, sizeof(window_vertex), window_vertex, GL_STATIC_DRAW);

    glVertexAttribPointer(0, 2, GL_FLOAT, GL_FALSE, sizeof(GL_FLOAT)*4, 0);
    glVertexAttribPointer(1, 2, GL_FLOAT, GL_FALSE, sizeof(GL_FLOAT)*4, (const GLvoid*)(sizeof(GL_FLOAT)*2));

    glEnableVertexAttribArray(0);
    glEnableVertexAttribArray(1);
    
}

void initFBO()
{
    /* shadow fbo */
    glGenFramebuffers(1, &fbo_shadow);
    glBindFramebuffer(GL_FRAMEBUFFER, fbo_shadow);

    glGenTextures(1, &fboDataTexture_shadow);
    glBindTexture(GL_TEXTURE_2D, fboDataTexture_shadow);
    glTexImage2D(GL_TEXTURE_2D, 0, GL_DEPTH_COMPONENT32, 4096, 4096, 0, GL_DEPTH_COMPONENT, GL_FLOAT, NULL);
    glTexParameteri(GL_TEXTURE_2D, GL_TEXTURE_WRAP_S, GL_CLAMP_TO_EDGE);
    glTexParameteri(GL_TEXTURE_2D, GL_TEXTURE_WRAP_T, GL_CLAMP_TO_EDGE);
    glTexParameteri(GL_TEXTURE_2D, GL_TEXTURE_MIN_FILTER, GL_LINEAR);
    glTexParameteri(GL_TEXTURE_2D, GL_TEXTURE_MAG_FILTER, GL_LINEAR);
    glTexParameteri(GL_TEXTURE_2D, GL_TEXTURE_COMPARE_MODE, GL_COMPARE_REF_TO_TEXTURE);
    glTexParameteri(GL_TEXTURE_2D, GL_TEXTURE_COMPARE_FUNC, GL_LEQUAL);

    glFramebufferTexture(GL_FRAMEBUFFER, GL_DEPTH_ATTACHMENT, fboDataTexture_shadow, 0);
    
    /* window fbo */
    //FBO
    glGenFramebuffers(1, &fbo_window);

    //Depth RBO
    glGenRenderbuffers(1, &depthrbo_window);
    glBindRenderbuffer(GL_RENDERBUFFER, depthrbo_window);
    glRenderbufferStorage(GL_RENDERBUFFER, GL_DEPTH_COMPONENT32, FRAME_WIDTH, FRAME_HEIGHT);

    //fboDataTexture
    glGenTextures(1, &fboDataTexture_window);
    glBindTexture(GL_TEXTURE_2D, fboDataTexture_window);
    glTexImage2D(GL_TEXTURE_2D, 0, GL_RGBA, FRAME_WIDTH, FRAME_HEIGHT, 0, GL_RGBA,GL_UNSIGNED_BYTE, NULL );
    glTexParameteri(GL_TEXTURE_2D, GL_TEXTURE_WRAP_S, GL_CLAMP_TO_EDGE );
    glTexParameteri(GL_TEXTURE_2D, GL_TEXTURE_WRAP_T, GL_CLAMP_TO_EDGE );
    glTexParameteri(GL_TEXTURE_2D, GL_TEXTURE_MIN_FILTER, GL_LINEAR );
    glTexParameteri(GL_TEXTURE_2D, GL_TEXTURE_MAG_FILTER, GL_LINEAR );

    //Bind FBO
    glBindFramebuffer(GL_DRAW_FRAMEBUFFER, fbo_window);
    //Set depthrbo to current fbo
    glFramebufferRenderbuffer(GL_DRAW_FRAMEBUFFER, GL_DEPTH_ATTACHMENT, GL_RENDERBUFFER, depthrbo_window);
    //Set buffertexture to current fbo
    glFramebufferTexture2D(GL_FRAMEBUFFER, GL_COLOR_ATTACHMENT0, GL_TEXTURE_2D, fboDataTexture_window, 0);
    
    /* DOF fbo */
    //FBO
    glGenFramebuffers(1, &fbo_DOF);

    //Depth RBO
    glGenRenderbuffers(1, &depthrbo_DOF);
    glBindRenderbuffer(GL_RENDERBUFFER, depthrbo_DOF);
    glRenderbufferStorage(GL_RENDERBUFFER, GL_DEPTH_COMPONENT32, FRAME_WIDTH, FRAME_HEIGHT);

    //fboDataTexture
    glGenTextures(1, &fboDataTexture_DOF);
    glBindTexture(GL_TEXTURE_2D, fboDataTexture_DOF);
    glTexImage2D(GL_TEXTURE_2D, 0, GL_RGBA, FRAME_WIDTH, FRAME_HEIGHT, 0, GL_RGBA,GL_UNSIGNED_BYTE, NULL );
    glTexParameteri(GL_TEXTURE_2D, GL_TEXTURE_WRAP_S, GL_CLAMP_TO_EDGE );
    glTexParameteri(GL_TEXTURE_2D, GL_TEXTURE_WRAP_T, GL_CLAMP_TO_EDGE );
    glTexParameteri(GL_TEXTURE_2D, GL_TEXTURE_MIN_FILTER, GL_LINEAR );
    glTexParameteri(GL_TEXTURE_2D, GL_TEXTURE_MAG_FILTER, GL_LINEAR );

    //Bind FBO
    glBindFramebuffer(GL_DRAW_FRAMEBUFFER, fbo_DOF);
    //Set depthrbo to current fbo
    glFramebufferRenderbuffer(GL_DRAW_FRAMEBUFFER, GL_DEPTH_ATTACHMENT, GL_RENDERBUFFER, depthrbo_DOF);
    //Set buffertexture to current fbo
    glFramebufferTexture2D(GL_FRAMEBUFFER, GL_COLOR_ATTACHMENT0, GL_TEXTURE_2D, fboDataTexture_DOF, 0);
    
    /* Blur fbo */
    //FBO
    glGenFramebuffers(1, &fbo_blur);
    //Depth RBO
    glGenRenderbuffers(1, &depthrbo_blur);
    glBindRenderbuffer(GL_RENDERBUFFER, depthrbo_blur);
    glRenderbufferStorage(GL_RENDERBUFFER, GL_DEPTH_COMPONENT32, FRAME_WIDTH, FRAME_HEIGHT);

    //fboDataTexture
    glGenTextures(1, &fboDataTexture_blur);
    glBindTexture(GL_TEXTURE_2D, fboDataTexture_blur);
    glTexImage2D(GL_TEXTURE_2D, 0, GL_RGBA, FRAME_WIDTH, FRAME_HEIGHT, 0, GL_RGBA,GL_UNSIGNED_BYTE, NULL );
    glTexParameteri(GL_TEXTURE_2D, GL_TEXTURE_WRAP_S, GL_CLAMP_TO_EDGE);
    glTexParameteri(GL_TEXTURE_2D, GL_TEXTURE_WRAP_T, GL_CLAMP_TO_EDGE);
    glTexParameteri(GL_TEXTURE_2D, GL_TEXTURE_MIN_FILTER, GL_LINEAR );
    glTexParameteri(GL_TEXTURE_2D, GL_TEXTURE_MAG_FILTER, GL_LINEAR );

    //Bind FBO
    glBindFramebuffer(GL_DRAW_FRAMEBUFFER, fbo_blur);
    //Set depthrbo to current fbo
    glFramebufferRenderbuffer(GL_DRAW_FRAMEBUFFER, GL_DEPTH_ATTACHMENT, GL_RENDERBUFFER, depthrbo_blur);
    //Set buffertexture to current fbo
    glFramebufferTexture2D(GL_FRAMEBUFFER, GL_COLOR_ATTACHMENT0, GL_TEXTURE_2D, fboDataTexture_blur, 0);
    
    /* Bloom fbo */
    //FBO
    glGenFramebuffers(1, &fbo_bloom);
    //Depth RBO
    glGenRenderbuffers(1, &depthrbo_bloom);
    glBindRenderbuffer(GL_RENDERBUFFER, depthrbo_bloom);
    glRenderbufferStorage(GL_RENDERBUFFER, GL_DEPTH_COMPONENT32, FRAME_WIDTH, FRAME_HEIGHT);

    //fboDataTexture
    glGenTextures(1, &fboDataTexture_bloom);
    glBindTexture(GL_TEXTURE_2D, fboDataTexture_bloom);
    glTexImage2D(GL_TEXTURE_2D, 0, GL_RGBA, FRAME_WIDTH, FRAME_HEIGHT, 0, GL_RGBA,GL_UNSIGNED_BYTE, NULL );
    glTexParameteri(GL_TEXTURE_2D, GL_TEXTURE_WRAP_S, GL_CLAMP_TO_EDGE);
    glTexParameteri(GL_TEXTURE_2D, GL_TEXTURE_WRAP_T, GL_CLAMP_TO_EDGE);
    glTexParameteri(GL_TEXTURE_2D, GL_TEXTURE_MIN_FILTER, GL_LINEAR );
    glTexParameteri(GL_TEXTURE_2D, GL_TEXTURE_MAG_FILTER, GL_LINEAR );

    //Bind FBO
    glBindFramebuffer(GL_DRAW_FRAMEBUFFER, fbo_bloom);
    //Set depthrbo to current fbo
    glFramebufferRenderbuffer(GL_DRAW_FRAMEBUFFER, GL_DEPTH_ATTACHMENT, GL_RENDERBUFFER, depthrbo_bloom);
    //Set buffertexture to current fbo
    glFramebufferTexture2D(GL_FRAMEBUFFER, GL_COLOR_ATTACHMENT0, GL_TEXTURE_2D, fboDataTexture_bloom, 0);
}

void initializeGL()
{
    initScene();
    m_camera->InitViewMatrix(glm::vec3(512.0, 120.0, 512.0), glm::vec3(512.0, 140.0, 500.0));
    m_camera->InitProjectionMatrix(cam_fov, FRAME_WIDTH, FRAME_HEIGHT, cam_near_plane, cam_far_plane);
    w_camera->InitProjectionMatrix(cam_fov, FRAME_WIDTH, FRAME_HEIGHT, cam_near_plane, cam_far_plane);
    m_speed = 0.0;
    in_eye = glm::vec3(512.0, 120.0, 512.0);
    in_lookAt = glm::vec3(512.0, 140.0, 500.0);
    
    /* light */
    light_pos = vec3(400.0f, 1200.0f, 1000.0f);
    
    /* initialization */
    initFBO();
    initShader();
    initWindow();
}

void resizeGL(GLFWwindow *window, int w, int h)
{
    FRAME_WIDTH = w;
    FRAME_HEIGHT = h;
    m_camera->InitProjectionMatrix( cam_fov, FRAME_WIDTH, FRAME_HEIGHT, cam_near_plane, cam_far_plane);
    delete m_waterFB;
    m_waterFB = new WaterFrameBuffer(FRAME_WIDTH, FRAME_HEIGHT);
    delete m_deferFB;
    m_deferFB = new DeferFrameBuffer(FRAME_WIDTH, FRAME_HEIGHT);
    delete m_bloomFB;
    m_bloomFB = new BloomFrameBuffer(FRAME_WIDTH, FRAME_HEIGHT);
    glViewport(0, 0, FRAME_WIDTH, FRAME_HEIGHT);
    
    /* window fbo */
    // Depth RBO
    glDeleteRenderbuffers(1, &depthrbo_window);
    glGenRenderbuffers(1, &depthrbo_window);
    glBindRenderbuffer(GL_RENDERBUFFER, depthrbo_window);
    glRenderbufferStorage(GL_RENDERBUFFER, GL_DEPTH_COMPONENT32, FRAME_WIDTH, FRAME_HEIGHT);

    //fboDataTexture
    glDeleteTextures(1, &fboDataTexture_window);
    glGenTextures(1, &fboDataTexture_window);
    glBindTexture(GL_TEXTURE_2D, fboDataTexture_window);
    glTexImage2D(GL_TEXTURE_2D, 0, GL_RGBA, FRAME_WIDTH, FRAME_HEIGHT, 0, GL_RGBA,GL_UNSIGNED_BYTE, NULL );
    glTexParameteri(GL_TEXTURE_2D, GL_TEXTURE_WRAP_S, GL_CLAMP_TO_EDGE );
    glTexParameteri(GL_TEXTURE_2D, GL_TEXTURE_WRAP_T, GL_CLAMP_TO_EDGE );
    glTexParameteri(GL_TEXTURE_2D, GL_TEXTURE_MIN_FILTER, GL_LINEAR );
    glTexParameteri(GL_TEXTURE_2D, GL_TEXTURE_MAG_FILTER, GL_LINEAR );

    //Bind FBO
    glBindFramebuffer(GL_DRAW_FRAMEBUFFER, fbo_window);
    //Set depthrbo to current fbo
    glFramebufferRenderbuffer(GL_DRAW_FRAMEBUFFER, GL_DEPTH_ATTACHMENT, GL_RENDERBUFFER, depthrbo_window);
    //Set buffertexture to current fbo
    glFramebufferTexture2D(GL_FRAMEBUFFER, GL_COLOR_ATTACHMENT0, GL_TEXTURE_2D, fboDataTexture_window, 0);
    
    /* DOF fbo */
    // Depth RBO
    glDeleteRenderbuffers(1, &depthrbo_DOF);
    glGenRenderbuffers(1, &depthrbo_DOF);
    glBindRenderbuffer(GL_RENDERBUFFER, depthrbo_DOF);
    glRenderbufferStorage(GL_RENDERBUFFER, GL_DEPTH_COMPONENT32, FRAME_WIDTH, FRAME_HEIGHT);

    //fboDataTexture
    glDeleteTextures(1, &fboDataTexture_DOF);
    glGenTextures(1, &fboDataTexture_DOF);
    glBindTexture(GL_TEXTURE_2D, fboDataTexture_DOF);
    glTexImage2D(GL_TEXTURE_2D, 0, GL_RGBA, FRAME_WIDTH, FRAME_HEIGHT, 0, GL_RGBA,GL_UNSIGNED_BYTE, NULL );
    glTexParameteri(GL_TEXTURE_2D, GL_TEXTURE_WRAP_S, GL_CLAMP_TO_EDGE );
    glTexParameteri(GL_TEXTURE_2D, GL_TEXTURE_WRAP_T, GL_CLAMP_TO_EDGE );
    glTexParameteri(GL_TEXTURE_2D, GL_TEXTURE_MIN_FILTER, GL_LINEAR );
    glTexParameteri(GL_TEXTURE_2D, GL_TEXTURE_MAG_FILTER, GL_LINEAR );

    //Bind FBO
    glBindFramebuffer(GL_DRAW_FRAMEBUFFER, fbo_DOF);
    //Set depthrbo to current fbo
    glFramebufferRenderbuffer(GL_DRAW_FRAMEBUFFER, GL_DEPTH_ATTACHMENT, GL_RENDERBUFFER, depthrbo_DOF);
    //Set buffertexture to current fbo
    glFramebufferTexture2D(GL_FRAMEBUFFER, GL_COLOR_ATTACHMENT0, GL_TEXTURE_2D, fboDataTexture_DOF, 0);
    
    /* Blur fbo */
    // Depth RBO
    glDeleteRenderbuffers(1, &depthrbo_blur);
    glGenRenderbuffers(1, &depthrbo_blur);
    glBindRenderbuffer(GL_RENDERBUFFER, depthrbo_blur);
    glRenderbufferStorage(GL_RENDERBUFFER, GL_DEPTH_COMPONENT32, FRAME_WIDTH, FRAME_HEIGHT);

    //fboDataTexture
    glDeleteTextures(1, &fboDataTexture_blur);
    glGenTextures(1, &fboDataTexture_blur);
    glBindTexture(GL_TEXTURE_2D, fboDataTexture_blur);
    glTexImage2D(GL_TEXTURE_2D, 0, GL_RGBA, FRAME_WIDTH, FRAME_HEIGHT, 0, GL_RGBA,GL_UNSIGNED_BYTE, NULL );
    glTexParameteri(GL_TEXTURE_2D, GL_TEXTURE_WRAP_S, GL_CLAMP_TO_EDGE);
    glTexParameteri(GL_TEXTURE_2D, GL_TEXTURE_WRAP_T, GL_CLAMP_TO_EDGE);
    glTexParameteri(GL_TEXTURE_2D, GL_TEXTURE_MIN_FILTER, GL_LINEAR );
    glTexParameteri(GL_TEXTURE_2D, GL_TEXTURE_MAG_FILTER, GL_LINEAR );

    //Bind FBO
    glBindFramebuffer(GL_DRAW_FRAMEBUFFER, fbo_blur);
    //Set depthrbo to current fbo
    glFramebufferRenderbuffer(GL_DRAW_FRAMEBUFFER, GL_DEPTH_ATTACHMENT, GL_RENDERBUFFER, depthrbo_blur);
    //Set buffertexture to current fbo
    glFramebufferTexture2D(GL_FRAMEBUFFER, GL_COLOR_ATTACHMENT0, GL_TEXTURE_2D, fboDataTexture_blur, 0);
    
    /* Bloom fbo */
    // Depth RBO
    glDeleteRenderbuffers(1, &depthrbo_bloom);
    glGenRenderbuffers(1, &depthrbo_bloom);
    glBindRenderbuffer(GL_RENDERBUFFER, depthrbo_bloom);
    glRenderbufferStorage(GL_RENDERBUFFER, GL_DEPTH_COMPONENT32, FRAME_WIDTH, FRAME_HEIGHT);

    //fboDataTexture
    glDeleteTextures(1, &fboDataTexture_bloom);
    glGenTextures(1, &fboDataTexture_bloom);
    glBindTexture(GL_TEXTURE_2D, fboDataTexture_bloom);
    glTexImage2D(GL_TEXTURE_2D, 0, GL_RGBA, FRAME_WIDTH, FRAME_HEIGHT, 0, GL_RGBA,GL_UNSIGNED_BYTE, NULL );
    glTexParameteri(GL_TEXTURE_2D, GL_TEXTURE_WRAP_S, GL_CLAMP_TO_EDGE);
    glTexParameteri(GL_TEXTURE_2D, GL_TEXTURE_WRAP_T, GL_CLAMP_TO_EDGE);
    glTexParameteri(GL_TEXTURE_2D, GL_TEXTURE_MIN_FILTER, GL_LINEAR );
    glTexParameteri(GL_TEXTURE_2D, GL_TEXTURE_MAG_FILTER, GL_LINEAR );

    //Bind FBO
    glBindFramebuffer(GL_DRAW_FRAMEBUFFER, fbo_bloom);
    //Set depthrbo to current fbo
    glFramebufferRenderbuffer(GL_DRAW_FRAMEBUFFER, GL_DEPTH_ATTACHMENT, GL_RENDERBUFFER, depthrbo_bloom);
    //Set buffertexture to current fbo
    glFramebufferTexture2D(GL_FRAMEBUFFER, GL_COLOR_ATTACHMENT0, GL_TEXTURE_2D, fboDataTexture_bloom, 0);
    
    
}

void updateState()
{
    // [TODO] update your eye position and look-at center here
    m_camera->Move(m_speed);
    
    // adjust camera position with terrain
    adjustCameraPositionWithTerrain();

    // update airplane
    updateAirplane(m_camera->view_matrix);
}
    
void paintGL(){

    // [TODO] implement your rendering function here
    
    /* ******************** shadow matrix ********************************** */
    const float shadow_range = 800.0f;
    mat4 light_proj_matrix = ortho(-shadow_range, shadow_range, -shadow_range, shadow_range, 0.0f, 8000.0f);
    mat4 light_view_matrix = lookAt(light_pos, vec3(0.0f, 0.0f, 0.0f), vec3(0.0f, 1.0f, 0.0f));
    mat4 light_vp_matrix = light_proj_matrix * light_view_matrix;

    mat4 scale_bias_matrix = translate(mat4(1.0f), vec3(0.5f, 0.5f, 0.5f)) * scale(mat4(1.0f), vec3(0.5f, 0.5f, 0.5f));
    mat4 shadow_sbpv_matrix = scale_bias_matrix * light_vp_matrix;
    
    /* ******************** shadow **************************************************** */
    
    /* airplane */
    glBindFramebuffer(GL_FRAMEBUFFER, fbo_shadow);
    glEnable(GL_DEPTH_TEST);
    glClear(GL_DEPTH_BUFFER_BIT);
    glViewport(0, 0, 4096, 4096);
    glEnable(GL_POLYGON_OFFSET_FILL);
    glPolygonOffset(4.0f, 4.0f);
    shadow_program->useShader();

    mat4 T, S, R;
    T = translate(mat4(1.0), m_airplanePosition);
    S = scale(mat4(1.0), m_airplaneManager->models[0].scale);
    R = m_airplaneRotMat;
    mat4 model_matrix = T * R * S;

    glUniformMatrix4fv(um4mvp_shadow_LOC, 1, GL_FALSE, value_ptr(light_vp_matrix * model_matrix));
    glUniformMatrix4fv(um4m_LOC, 1, GL_FALSE, value_ptr(model_matrix));

    for (int j = 0 ; j < m_airplaneManager->models[0].shapes.size(); j++)
    {
        glBindVertexArray(m_airplaneManager->models[0].shapes[j].vao);
        m_airplaneManager->models[0].materials[m_airplaneManager->models[0].shapes[j].materialID].diff_tex->bind();
        glBindBuffer(GL_ELEMENT_ARRAY_BUFFER, m_airplaneManager->models[0].shapes[j].ibo);
        glDrawElements(GL_TRIANGLES, m_airplaneManager->models[0].shapes[j].drawCount, GL_UNSIGNED_INT, 0);
    }
    
    /* obj */
    for (int i = 0; i < m_objManager->models.size(); i++)
    {
        T = glm::translate(glm::mat4(1.0), m_objManager->models[i].position);
        S = glm::scale(glm::mat4(1.0), m_objManager->models[i].scale);
        R = glm::rotate(glm::mat4(1.0), m_objManager->models[i].rotation.y, glm::vec3(0.0, 1.0, 0.0));
        model_matrix = T * R * S;

        glUniformMatrix4fv(um4mvp_shadow_LOC, 1, GL_FALSE, value_ptr(light_vp_matrix * model_matrix));
        glUniformMatrix4fv(um4m_LOC, 1, GL_FALSE, value_ptr(model_matrix));

        for (int j = 0 ; j < m_objManager->models[i].shapes.size(); j++)
        {
            glBindVertexArray(m_objManager->models[i].shapes[j].vao);
            m_objManager->models[i].materials[m_objManager->models[i].shapes[j].materialID].diff_tex->bind();
            m_objManager->models[i].materials[m_objManager->models[i].shapes[j].materialID].normal_tex->bind();
            glBindBuffer(GL_ELEMENT_ARRAY_BUFFER, m_objManager->models[i].shapes[j].ibo);
            glDrawElements(GL_TRIANGLES, m_objManager->models[i].shapes[j].drawCount, GL_UNSIGNED_INT, 0);
        }
    }
    
    tree_shadow_program->useShader();
    /* plant */
    for (int i = 0; i < 4; i++)
    {
        S = glm::scale(glm::mat4(1.0), m_plantManager->models[i].scale);
        R = glm::rotate(glm::mat4(1.0), deg2rad(m_plantManager->models[i].rotation.x), glm::vec3(1.0, 0.0, 0.0));
        model_matrix = R * S;
        
        glUniformMatrix4fv(tree_um4vp_shadow_LOC, 1, GL_FALSE, value_ptr(light_vp_matrix));
        glUniformMatrix4fv(tree_um4m_LOC, 1, GL_FALSE, value_ptr(model_matrix));
        glUniform1i(tree_tex, 3);
        glActiveTexture(GL_TEXTURE3);
        
        for (int j = 0 ; j < m_plantManager->models[i].shapes.size(); j++)
        {
            glBindVertexArray(m_plantManager->models[i].shapes[j].vao);
            m_plantManager->models[i].materials[m_plantManager->models[i].shapes[j].materialID].diff_tex->bind();
            glBindBuffer(GL_ELEMENT_ARRAY_BUFFER, m_plantManager->models[i].shapes[j].ibo);
            if (i < 2)
                glDrawElementsInstanced(GL_TRIANGLES, m_plantManager->models[i].shapes[j].drawCount, GL_UNSIGNED_INT, 0, m_plantManager->m_numPlantInstance[0]);
            else
                glDrawElementsInstanced(GL_TRIANGLES, m_plantManager->models[i].shapes[j].drawCount, GL_UNSIGNED_INT, 0, m_plantManager->m_numPlantInstance[1]);
        }
    }
    
    glDisable(GL_POLYGON_OFFSET_FILL);
    /* ********************* water FBO ************************* */
    /* reflection */
    m_waterFB->BindReflectionBuffer();
    m_waterFB->Clear();
    glEnable(GL_CLIP_DISTANCE0);
    glm::vec4 clip_plane = glm::vec4(0, 1, 0, -m_waterManager->GetHeight());
    float dis = 2 * (m_camera->position.y - m_waterManager->GetHeight());
    glm::vec3 w_eye = glm::vec3(m_camera->position.x, m_camera->position.y - dis, m_camera->position.z);
    float w_pitch = -1 * m_camera->pitch;
    glm::vec3 front = glm::vec3(cos(deg2rad(m_camera->yaw))*cos(deg2rad(w_pitch)), sin(deg2rad(w_pitch)), sin(deg2rad(m_camera->yaw)) * cos(deg2rad(w_pitch)));
    glm::vec3 w_lookAt = w_eye + glm::normalize(front);
    w_camera->InitViewMatrix(w_eye, w_lookAt);
    w_camera->proj_matirx = glm::scale(m_camera->proj_matirx, glm::vec3(1.0, -1.0, 1.0));
    m_terrain->render(w_camera, clip_plane, shadow_sbpv_matrix, fboDataTexture_shadow, 0, pointLightOpen);
    m_airplaneManager->render(w_camera, m_airplanePosition, m_airplaneRotMat, fboDataTexture_shadow, shadow_sbpv_matrix, clip_plane, 0, pointLightOpen);
    m_objManager->render(w_camera, fboDataTexture_shadow, shadow_sbpv_matrix, clip_plane, 0, pointLightOpen);
    m_plantManager->render(w_camera, clip_plane, fboDataTexture_shadow, shadow_sbpv_matrix, 0, pointLightOpen);
    m_sphere->render(w_camera, 0);
    /* refraction */
    m_waterFB->BindRefractionBuffer();
    m_waterFB->Clear();
    glEnable(GL_CLIP_DISTANCE0);
    clip_plane = glm::vec4(0, -1, 0, m_waterManager->GetHeight());
    m_terrain->render(m_camera, clip_plane, shadow_sbpv_matrix, fboDataTexture_shadow, 0, pointLightOpen);
    
    /* ******************** DOF fbo  *************************************************************************** */
    if (effectMode == DOF)
    {
        glBindFramebuffer(GL_FRAMEBUFFER, fbo_window);
        glDrawBuffer(GL_COLOR_ATTACHMENT0);
        glClear(GL_COLOR_BUFFER_BIT | GL_DEPTH_BUFFER_BIT);
        glViewport(0, 0, FRAME_WIDTH, FRAME_HEIGHT);
        glEnable(GL_DEPTH_TEST);
        glDisable(GL_CLIP_DISTANCE0);

        m_terrain->render(m_camera, clip_plane, shadow_sbpv_matrix, fboDataTexture_shadow, 1, pointLightOpen);
        m_airplaneManager->render(m_camera, m_airplanePosition, m_airplaneRotMat, fboDataTexture_shadow, shadow_sbpv_matrix, clip_plane, 1, pointLightOpen);
        m_objManager->render(m_camera, fboDataTexture_shadow, shadow_sbpv_matrix, clip_plane, 1, pointLightOpen);
        m_waterManager->render(m_waterFB, m_camera, 1);
        m_plantManager->render(m_camera, clip_plane, fboDataTexture_shadow, shadow_sbpv_matrix, 1, pointLightOpen);
        m_sphere->render(m_camera, 1);

        /* DOF */
        glBindFramebuffer(GL_FRAMEBUFFER, fbo_DOF);
        glDrawBuffer(GL_COLOR_ATTACHMENT0);
        glClear(GL_COLOR_BUFFER_BIT | GL_DEPTH_BUFFER_BIT);
        glViewport(0, 0, FRAME_WIDTH, FRAME_HEIGHT);

        DOF_program->useShader();

        glBindVertexArray(window_vao);
        glUniform1i(tex_DOF_LOC, 6);
        glActiveTexture(GL_TEXTURE6);
        glBindTexture(GL_TEXTURE_2D, fboDataTexture_window);

        glDrawArrays(GL_TRIANGLE_FAN, 0, 4);

        m_windowManager->render(fboDataTexture_DOF, FRAME_WIDTH, FRAME_HEIGHT);
    }
    /* ******************** defer shading *************************************************************************** */
    else if(effectMode >= DEFER_POS && effectMode <= DEFER_SPECULAR) {
        // [Note]: first pass render
        m_deferFB->BindGeoBuffer();
        m_deferFB->Clear();
        const GLenum draw_buffers[] = { GL_COLOR_ATTACHMENT0, GL_COLOR_ATTACHMENT1, GL_COLOR_ATTACHMENT2, GL_COLOR_ATTACHMENT3, GL_COLOR_ATTACHMENT4, GL_COLOR_ATTACHMENT5};
        glDrawBuffers(6, draw_buffers);
        glDisable(GL_CLIP_DISTANCE0);
        
        m_terrain->render(m_camera, clip_plane, shadow_sbpv_matrix, fboDataTexture_shadow, 0, pointLightOpen);
        m_airplaneManager->render(m_camera, m_airplanePosition, m_airplaneRotMat, fboDataTexture_shadow, shadow_sbpv_matrix, clip_plane, 0, pointLightOpen);
        m_objManager->render(m_camera, fboDataTexture_shadow, shadow_sbpv_matrix, clip_plane, 0, pointLightOpen);
        m_waterManager->render(m_waterFB, m_camera, 0);
        m_plantManager->render(m_camera, clip_plane, fboDataTexture_shadow, shadow_sbpv_matrix, 0, pointLightOpen);
        m_sphere->render(m_camera, 0);
        
        switch (effectMode) {
            case DEFER_POS:
                m_windowManager->render(m_deferFB->GetVertexTexture(), FRAME_WIDTH, FRAME_HEIGHT);
                break;
            case DEFER_NORMAL:
                m_windowManager->render(m_deferFB->GetNormalTexture(), FRAME_WIDTH, FRAME_HEIGHT);
                break;
            case DEFER_AMBIENT:
                m_windowManager->render(m_deferFB->GetAmbientTexture(), FRAME_WIDTH, FRAME_HEIGHT);
                break;
            case DEFER_DIFFUSE:
                m_windowManager->render(m_deferFB->GetDiffuseTexture(), FRAME_WIDTH, FRAME_HEIGHT);
                break;
            case DEFER_SPECULAR:
                m_windowManager->render(m_deferFB->GetSpecularTexture(), FRAME_WIDTH, FRAME_HEIGHT);
                break;
            default:
                m_windowManager->render(m_deferFB->GetSceneTexture(), FRAME_WIDTH, FRAME_HEIGHT);
                break;
        }
    }
    /* ******************** bloom effect *************************************************************************** */
    else if(effectMode == BLOOM) {
        m_bloomFB->BindblurBuffer();
        m_bloomFB->Clear();
        const GLenum draw_buffers[] = {GL_COLOR_ATTACHMENT0, GL_COLOR_ATTACHMENT1, GL_COLOR_ATTACHMENT2, GL_COLOR_ATTACHMENT3, GL_COLOR_ATTACHMENT4, GL_COLOR_ATTACHMENT5, GL_COLOR_ATTACHMENT6};
        glDrawBuffers(7, draw_buffers);
        glDisable(GL_CLIP_DISTANCE0);
        // [Note]: first pass render(draw: scene & light mask)
        m_terrain->render(m_camera, clip_plane, shadow_sbpv_matrix, fboDataTexture_shadow, 0, pointLightOpen);
        m_airplaneManager->render(m_camera, m_airplanePosition, m_airplaneRotMat, fboDataTexture_shadow, shadow_sbpv_matrix, clip_plane, 0, pointLightOpen);
        m_objManager->render(m_camera, fboDataTexture_shadow, shadow_sbpv_matrix, clip_plane, 0, pointLightOpen);
        m_waterManager->render(m_waterFB, m_camera, 0);
        m_plantManager->render(m_camera, clip_plane, fboDataTexture_shadow, shadow_sbpv_matrix, 0, pointLightOpen);
        m_sphere->render(m_camera, 0);
        
        // [Note]: second pass render (draw: 2 blur from light part filter)
        glBindFramebuffer(GL_FRAMEBUFFER, fbo_blur);
        glViewport(0, 0, FRAME_WIDTH, FRAME_HEIGHT);
        glDrawBuffer(GL_COLOR_ATTACHMENT0);
        glClearColor(1.0, 1.0, 1.0, 1.0);
        glClear(GL_COLOR_BUFFER_BIT | GL_DEPTH_BUFFER_BIT);
        
        blur_program->useShader();

        glBindVertexArray(window_vao);
        glUniform1i(tex_blur_LOC, 6);
        glActiveTexture(GL_TEXTURE6);
        glBindTexture(GL_TEXTURE_2D, m_bloomFB->GetBallMaskTexture());

        glDrawArrays(GL_TRIANGLE_FAN, 0, 4);
        
        // [Note]: Third pass render (draw: blend window & blur1 & blur2 together)
        glBindFramebuffer(GL_FRAMEBUFFER, fbo_bloom);
        glViewport(0, 0, FRAME_WIDTH, FRAME_HEIGHT);
        glDrawBuffer(GL_COLOR_ATTACHMENT0);
        glClearColor(1.0, 1.0, 1.0, 1.0);
        glClear(GL_COLOR_BUFFER_BIT | GL_DEPTH_BUFFER_BIT);
        
        bloom_program->useShader();

        glBindVertexArray(window_vao);
        glUniform1i(tex_bloom_scene_LOC, 5);
        glActiveTexture(GL_TEXTURE5);
        glBindTexture(GL_TEXTURE_2D, m_bloomFB->GetSceneTexture());
        glUniform1i(tex_bloom_blur_LOC, 6);
        glActiveTexture(GL_TEXTURE6);
        glBindTexture(GL_TEXTURE_2D, fboDataTexture_blur);

        glDrawArrays(GL_TRIANGLE_FAN, 0, 4);
        
        // [Note]: Final pass
        m_windowManager->render(fboDataTexture_bloom, FRAME_WIDTH, FRAME_HEIGHT);
    }
    /* *********************************** origin render ******************************************* */
    else
    {
        glBindFramebuffer(GL_FRAMEBUFFER, 0);
        glViewport(0, 0, FRAME_WIDTH, FRAME_HEIGHT);
        glEnable(GL_DEPTH_TEST);
        glClearColor(1.0, 1.0, 1.0, 1.0);
        glClear(GL_COLOR_BUFFER_BIT | GL_DEPTH_BUFFER_BIT);
        glDisable(GL_CLIP_DISTANCE0);

        m_terrain->render(m_camera, clip_plane, shadow_sbpv_matrix, fboDataTexture_shadow, 0, pointLightOpen);
        m_airplaneManager->render(m_camera, m_airplanePosition, m_airplaneRotMat, fboDataTexture_shadow, shadow_sbpv_matrix, clip_plane, 0, pointLightOpen);
        m_objManager->render(m_camera, fboDataTexture_shadow, shadow_sbpv_matrix, clip_plane, 0, pointLightOpen);
        m_waterManager->render(m_waterFB, m_camera, 0);
        m_plantManager->render(m_camera, clip_plane, fboDataTexture_shadow, shadow_sbpv_matrix, 0, pointLightOpen);
        m_sphere->render(m_camera, 0);
    }
    
    My_GUI();
    
}

////////////////////////////////////////////////
void mouseButtonCallback(GLFWwindow* window, int button, int action, int mods){
    
    if (button == GLFW_MOUSE_BUTTON_LEFT) {
        if (action == GLFW_PRESS) {
            isPress = true;
        }
        else if (action == GLFW_RELEASE) {
            isPress = false;
        }
    }
}
void mouseScrollCallback(GLFWwindow *window, double xoffset, double yoffset) {}
void cursorPosCallback(GLFWwindow* window, double x, double y){
    
    if (isPress)
    {
        float xoffset = x - cursorPos[0];
        float yoffset = cursorPos[1] - y;
        float sensitive = 0.1f;
        
        xoffset *= sensitive;
        yoffset *= sensitive;
        m_camera->Rotate(xoffset, yoffset);
    }
    cursorPos[0] = x;
    cursorPos[1] = y;
}
void keyCallback(GLFWwindow* window, int key, int scancode, int action, int mods){

}

void My_GUI(){
    ImGui_ImplOpenGL3_NewFrame();
    ImGui_ImplGlfw_NewFrame();
    ImGui::NewFrame();
    
    ImGui::SetNextWindowSize(ImVec2(320, 250));
    ImGui::SetNextWindowPos(ImVec2(20, 20));
    ImGui::Begin("ImGui", &my_gui_active, ImGuiWindowFlags_MenuBar);
    
    float pos[3] = { in_eye.x, in_eye.y, in_eye.z };
    if (ImGui::InputFloat3("Eye pos", pos))
    {
        in_eye = glm::vec3(pos[0], pos[1], pos[2]);
    }
    float la[3] = { in_lookAt.x, in_lookAt.y, in_lookAt.z };
    if (ImGui::InputFloat3("Look at", la))
    {
        in_lookAt = glm::vec3(la[0], la[1], la[2]);
    }
    if (ImGui::MenuItem("Apply!"))
    {
        m_camera->position = in_eye;
        m_camera->lookAt = in_lookAt;
    }
    float sd = m_speed;
    if (ImGui::SliderFloat("Speed", &sd, -0.3f, 0.3f))
    {
        m_speed = sd;
    }
    
    if (ImGui::MenuItem("Restart"))
    {
        m_camera->InitViewMatrix(glm::vec3(512.0, 120.0, 512.0), glm::vec3(512.0, 140.0, 500.0));
        m_speed = 0.0;
    }
    if (ImGui::MenuItem("Stop"))
    {
        m_speed = 0.0;
    }
    if (ImGui::MenuItem("Normal"))
    {
        m_objManager->normalSwitch();
    }
    if (ImGui::MenuItem("DOF"))
    {
        if(effectMode == DOF) effectMode = 0;
        else effectMode = DOF;
        std::cout << effectMode;
    }
    if (ImGui::BeginMenu("Defer"))
    {
        if (ImGui::MenuItem("POS"))
        {
            if(effectMode == DEFER_POS) {
                effectMode = 0;
            }
            else effectMode = DEFER_POS;
        }
        if (ImGui::MenuItem("NORMAL"))
        {
            if(effectMode == DEFER_NORMAL) {
                effectMode = 0;
            }
            else effectMode = DEFER_NORMAL;
        }

        if (ImGui::MenuItem("AMBIENT"))
        {
            if(effectMode == DEFER_AMBIENT) {
                effectMode = 0;
            }
            else effectMode = DEFER_AMBIENT;
        }

        if (ImGui::MenuItem("DIFFUSE"))
        {
            if(effectMode == DEFER_DIFFUSE) {
                effectMode = 0;
            }
            else effectMode = DEFER_DIFFUSE;
        }

        if (ImGui::MenuItem("SPECULAR"))
        {
            if(effectMode == DEFER_SPECULAR) {
                effectMode = 0;
            }
            else effectMode = DEFER_SPECULAR;
        }
        ImGui::EndMenu();
    }
    if (ImGui::MenuItem("Bloom"))
    {
        if(effectMode == BLOOM) {
            effectMode = 0;
            pointLightOpen = false;
        }
        else
        {
            effectMode = BLOOM;
            pointLightOpen = true;
        }
    }
    ImGui::End();
    ImGui::Render();
    ImGui_ImplOpenGL3_RenderDrawData(ImGui::GetDrawData());
}
////////////////////////////////////////////////
// The following functions don't need to change
void initScene() {
    m_camera = new Camera();
    m_plantManager = new PlantManager();
    m_objManager = new ObjManager();
    m_airplaneManager = new AirPlaneManager();
    m_waterManager = new Water();
    m_waterFB = new WaterFrameBuffer(FRAME_WIDTH, FRAME_HEIGHT);
    m_terrain = new Terrain();
    m_windowManager = new WindowManager();
    m_sphere = new SphereManager();
    
    w_camera = new Camera();
    /* defer */
    m_deferFB = new DeferFrameBuffer(FRAME_WIDTH, FRAME_HEIGHT);
    /* bloom */
    m_bloomFB = new BloomFrameBuffer(FRAME_WIDTH, FRAME_HEIGHT);
}
void updateAirplane(const glm::mat4 &viewMatrix) {
    // apply yaw
    glm::mat4 rm = viewMatrix;
    rm = glm::transpose(rm);
    glm::vec3 tangent(-1.0 * rm[2].x, 0.0, -1.0 * rm[2].z);
    tangent = glm::normalize(tangent);
    glm::vec3 bitangent = glm::normalize(glm::cross(glm::vec3(0.0, 1.0, 0.0), tangent));
    glm::vec3 normal = glm::normalize(glm::cross(tangent, bitangent));

    m_airplaneRotMat[0] = glm::vec4(bitangent, 0.0);
    m_airplaneRotMat[1] = glm::vec4(normal, 0.0);
    m_airplaneRotMat[2] = glm::vec4(tangent, 0.0);
    m_airplaneRotMat[3] = glm::vec4(0.0, 0.0, 0.0, 1.0);

    m_airplanePosition = m_camera->lookAt;
}

// adjust camera position and look-at center with terrain
void adjustCameraPositionWithTerrain() {
    // adjust camera height
    glm::vec3 cp = m_camera->lookAt;
    float ty = m_terrain->getHeight(cp.x, cp.z);
    if (cp.y < ty + 3.0) {
        m_camera->lookAt.y = ty + 3.0;
        m_camera->position.y = m_camera->position.y + (ty + 3.0 - cp.y);
    }
    cp = m_camera->position;
    ty = m_terrain->getHeight(cp.x, cp.z);
    if (cp.y < ty + 3.0) {
        m_camera->lookAt.y = ty + 3.0;
        m_camera->position.y = m_camera->position.y + (ty + 3.0 - cp.y);
    }
}
