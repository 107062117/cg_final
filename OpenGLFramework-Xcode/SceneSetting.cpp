#include "SceneSetting.h"

using namespace glm;
using namespace std;

PlantManager::PlantManager() {
    auto tree0 = appendPlants("assets/baum_hd_pine_pos.pos");
    this->m_numPlantInstance[0] = std::get<0>(tree0);
    this->m_plantInstancePositions[0] = std::get<1>(tree0);

    auto tree1 = appendPlants("assets/baum_hd_pos.pos");
    this->m_numPlantInstance[1] = std::get<0>(tree1);
    this->m_plantInstancePositions[1] = std::get<1>(tree1);

    auto grass0 = appendPlants("assets/grass_pos.pos");
    this->m_numPlantInstance[2] = std::get<0>(grass0);
    this->m_plantInstancePositions[2] = std::get<1>(grass0);

    auto grass2 = appendPlants("assets/grass2_pos.pos");
    this->m_numPlantInstance[3] = std::get<0>(grass2);
    this->m_plantInstancePositions[3] = std::get<1>(grass2);
    
    
    // Load model
    this->models.push_back(LoadModels("models/trees/baum_hd_leaves.obj", 0));
    this->models.push_back(LoadModels("models/trees/baum_hd_trunk.obj", 0));
    this->models.push_back(LoadModels("models/trees/baum_hd_pine_leaves.obj", 1));
    this->models.push_back(LoadModels("models/trees/baum_hd_pine_trunk.obj", 1));
    Model m = LoadModels("models/trees/grass.obj", 2);
    m.rotation = glm::vec3(0.0, 0.0, 0.0);
    this->models.push_back(m);
    m = LoadModels("models/trees/grass2.obj", 3);
    m.rotation = glm::vec3(0.0, 0.0, 0.0);
    this->models.push_back(m);
    
    // setup program
    this->grass_shader = new Shader("shader/vertex_grass.vs.glsl", "shader/fragment_grass.fs.glsl");
    this->grass_shader->useShader();
    GLuint programId = this->grass_shader->getProgramID();
    this->grass_uniform.m_mat = glGetUniformLocation(programId, "m_mat");
    this->grass_uniform.v_mat = glGetUniformLocation(programId, "v_mat");
    this->grass_uniform.p_mat = glGetUniformLocation(programId, "p_mat");
    this->grass_uniform.tex = glGetUniformLocation(programId, "tex");
    this->grass_uniform.clip_plane = glGetUniformLocation(programId, "plane");
    this->grass_uniform.renderBlur = glGetUniformLocation(programId, "renderBlur");
    /* defer */
    this->grass_uniform.Ka_LOC = glGetUniformLocation(programId, "Ka");
    this->grass_uniform.Kd_LOC = glGetUniformLocation(programId, "Kd");
    this->grass_uniform.Ks_LOC = glGetUniformLocation(programId, "Ks");
    glUniform1i(this->grass_uniform.tex, 3);
    
    this->grass_uniform.pointLightOpen_LOC = glGetUniformLocation(programId, "pointLightOpen");

    // setup tree program
    this->tree_shader = new Shader("shader/vertex_tree.vs.glsl", "shader/fragment_tree.fs.glsl");
    this->tree_shader->useShader();
    programId = this->tree_shader->getProgramID();
    
    this->tree_uniform.m_mat = glGetUniformLocation(programId, "m_mat");
    this->tree_uniform.v_mat = glGetUniformLocation(programId, "v_mat");
    this->tree_uniform.p_mat = glGetUniformLocation(programId, "p_mat");
    this->tree_uniform.tex = glGetUniformLocation(programId, "tex");
    this->tree_uniform.clip_plane = glGetUniformLocation(programId, "plane");
    glUniform1i(this->tree_uniform.tex, 3);
    this->tree_uniform.tex_shadow = glGetUniformLocation(programId, "tex_shadow");
    glUniform1i(this->tree_uniform.tex_shadow, 4);
    this->tree_uniform.um4shadow_LOC = glGetUniformLocation(programId, "um4shadow");
    this->tree_uniform.renderBlur = glGetUniformLocation(programId, "renderBlur");
    /* defer */
    this->tree_uniform.Ka_LOC = glGetUniformLocation(programId, "Ka");
    this->tree_uniform.Kd_LOC = glGetUniformLocation(programId, "Kd");
    this->tree_uniform.Ks_LOC = glGetUniformLocation(programId, "Ks");
    
    this->tree_uniform.pointLightOpen_LOC = glGetUniformLocation(programId, "pointLightOpen");

}

void PlantManager::render(Camera *camera, glm::vec4 plane, GLuint fboDataTexture_shadow, glm::mat4 shadow_sbpv_matrix, bool blur, bool pointLightOpen)
{
    // render grass
    this->grass_shader->useShader();
    for (int i = 4; i < 6; i++)
    {
        glm::mat4 S, R;
        S = glm::scale(glm::mat4(1.0), this->models[i].scale);
        R = glm::rotate(glm::mat4(1.0), deg2rad(this->models[i].rotation.x), glm::vec3(1.0, 0.0, 0.0));
        glm::mat4 model_matrix = R * S;
        glUniformMatrix4fv(this->grass_uniform.m_mat, 1, GL_FALSE, &model_matrix[0][0]);
        glUniformMatrix4fv(this->grass_uniform.v_mat, 1, GL_FALSE, value_ptr(camera->view_matrix));
        glUniformMatrix4fv(this->grass_uniform.p_mat, 1, GL_FALSE, value_ptr(camera->proj_matirx));

        glUniform4fv(this->grass_uniform.clip_plane, 1, &plane[0]);
        glUniform1i(this->grass_uniform.tex, 3);
        glUniform1i(this->grass_uniform.renderBlur, blur);
        glUniform1i(this->grass_uniform.pointLightOpen_LOC, pointLightOpen);


        for (int j = 0 ; j < this->models[i].shapes.size(); j++)
        {
            glBindVertexArray(this->models[i].shapes[j].vao);
            this->models[i].materials[this->models[i].shapes[j].materialID].diff_tex->bind();
            /* defer */
            glUniform4fv(this->grass_uniform.Ka_LOC, 1, &this->models[i].materials[this->models[i].shapes[j].materialID].Ka[0]);
            glUniform4fv(this->grass_uniform.Kd_LOC, 1, &this->models[i].materials[this->models[i].shapes[j].materialID].Kd[0]);
            glUniform4fv(this->grass_uniform.Ks_LOC, 1, &this->models[i].materials[this->models[i].shapes[j].materialID].Ks[0]);

            glBindBuffer(GL_ELEMENT_ARRAY_BUFFER, this->models[i].shapes[j].ibo);
            glDrawElementsInstanced(GL_TRIANGLES, this->models[i].shapes[j].drawCount, GL_UNSIGNED_INT, 0, this->m_numPlantInstance[i - 2]);
        }
    }
    // render tree
    this->tree_shader->useShader();
    for (int i = 0; i < 4; i++)
    {
        glm::mat4 S, R;
        S = glm::scale(glm::mat4(1.0), this->models[i].scale);
        R = glm::rotate(glm::mat4(1.0), deg2rad(this->models[i].rotation.x), glm::vec3(1.0, 0.0, 0.0));
        glm::mat4 model_matrix = R * S;
        glUniformMatrix4fv(this->tree_uniform.m_mat, 1, GL_FALSE, &model_matrix[0][0]);
        glUniformMatrix4fv(this->tree_uniform.v_mat, 1, GL_FALSE, value_ptr(camera->view_matrix));
        glUniformMatrix4fv(this->tree_uniform.p_mat, 1, GL_FALSE, value_ptr(camera->proj_matirx));
        
        glUniform4fv(this->tree_uniform.clip_plane, 1, &plane[0]);
        glUniform1i(this->tree_uniform.tex, 3);
        glUniform1i(this->tree_uniform.renderBlur, blur);
        glUniform1i(this->tree_uniform.pointLightOpen_LOC, pointLightOpen);
        
        /* ********** shadow ************************************************** */
        glUniform1i(this->tree_uniform.tex_shadow, 4);
        glActiveTexture(GL_TEXTURE4);
        glBindTexture(GL_TEXTURE_2D, fboDataTexture_shadow);
        
        glm::mat4 shadow_matrix = shadow_sbpv_matrix;
        glUniformMatrix4fv(this->tree_uniform.um4shadow_LOC, 1, GL_FALSE, value_ptr(shadow_matrix));
        /* ******************************************************************** */
        
        for (int j = 0 ; j < this->models[i].shapes.size(); j++)
        {
            glBindVertexArray(this->models[i].shapes[j].vao);
            this->models[i].materials[this->models[i].shapes[j].materialID].diff_tex->bind();
            /* defer */
            glUniform4fv(this->tree_uniform.Ka_LOC, 1, &this->models[i].materials[this->models[i].shapes[j].materialID].Ka[0]);
            glUniform4fv(this->tree_uniform.Kd_LOC, 1, &this->models[i].materials[this->models[i].shapes[j].materialID].Kd[0]);
            glUniform4fv(this->tree_uniform.Ks_LOC, 1, &this->models[i].materials[this->models[i].shapes[j].materialID].Ks[0]);
            glBindBuffer(GL_ELEMENT_ARRAY_BUFFER, this->models[i].shapes[j].ibo);
            if (i < 2)
                glDrawElementsInstanced(GL_TRIANGLES, this->models[i].shapes[j].drawCount, GL_UNSIGNED_INT, 0, m_numPlantInstance[0]);
            else
                glDrawElementsInstanced(GL_TRIANGLES, this->models[i].shapes[j].drawCount, GL_UNSIGNED_INT, 0, m_numPlantInstance[1]);
        }
    }
}

PlantManager::~PlantManager(){}

std::tuple<int, float*> PlantManager::appendPlants(const std::string &posFile) {
    // load positions
    int numPos = -1;
    std::ifstream input(posFile, std::ios::binary);
    if (!input.is_open()) {
        return{ -1, nullptr };
    }
    input.read((char*)(&numPos), sizeof(int));
    float *positions = new float[numPos * 3];
    input.read((char*)positions, numPos * 3 * sizeof(float));
    input.close();
    return{ numPos, positions };
}


PlantManager::Model PlantManager::LoadModels(std::string filePath, int index)
{
    Model model;
    const aiScene *scene = aiImportFile(filePath.c_str(), aiProcessPreset_TargetRealtime_MaxQuality);
    if(!scene) std::cout << "Fail load scene " + filePath+"\n";
    // load material texture
    for (unsigned int i = 0; i < scene->mNumMaterials; ++i)
    {
        aiMaterial *material = scene->mMaterials[i];
        PlantManager::Material Material;
        aiString texturePath;
        if (material->GetTexture(aiTextureType_DIFFUSE, 0, &texturePath) == aiReturn_SUCCESS)
        {
            Material.diff_tex = new TextureMaterial(std::string(texturePath.C_Str()));
            Material.diff_tex->setTextureUnit(GL_TEXTURE3);
            std::cout << "Load diff tex "+std::string(texturePath.C_Str())+"\n";
        }
        else
            std::cout << "Load texture fail "+std::string(texturePath.C_Str())+"\n";
        /* defer */
        aiColor3D color;
        material->Get(AI_MATKEY_COLOR_AMBIENT, color);
        Material.Ka = glm::vec4(color.r, color.g, color.b, 1.0);
        material->Get(AI_MATKEY_COLOR_DIFFUSE, color);
        Material.Kd = glm::vec4(color.r, color.g, color.b, 1.0);
        material->Get(AI_MATKEY_COLOR_SPECULAR, color);
        Material.Ks = glm::vec4(color.r, color.g, color.b, 1.0);
        
        model.materials.push_back(Material);
    }
    
    std::vector<Shape> shapes;
    for (unsigned int i = 0; i < scene->mNumMeshes; ++i)
    {
        aiMesh *mesh = scene->mMeshes[i];
        Shape shape;
        glGenVertexArrays(1, &shape.vao);
        glBindVertexArray(shape.vao);

        std::vector<float> vertices, normals, texcoords;
        // create 3 vbos to hold data
        for (unsigned int v = 0; v < mesh->mNumVertices; ++v)
        {
            vertices.push_back(mesh->mVertices[v][0]);
            vertices.push_back(mesh->mVertices[v][1]);
            vertices.push_back(mesh->mVertices[v][2]);
            texcoords.push_back(mesh->mTextureCoords[0][v][0]);
            texcoords.push_back(mesh->mTextureCoords[0][v][1]);
            normals.push_back(mesh->mNormals[v][0]);
            normals.push_back(mesh->mNormals[v][1]);
            normals.push_back(mesh->mNormals[v][2]);
        }
        std::vector<int> indices;
        // create 1 ibo to hold data
        for (unsigned int f = 0; f < mesh->mNumFaces; ++f)
        {
            indices.push_back(mesh->mFaces[f].mIndices[0]);
            indices.push_back(mesh->mFaces[f].mIndices[1]);
            indices.push_back(mesh->mFaces[f].mIndices[2]);
        }

        // glVertexAttribPointer / glEnableVertexArray calls...
        glGenBuffers(1, &shape.vbo_position);
        glBindBuffer(GL_ARRAY_BUFFER, shape.vbo_position);
        glBufferData(GL_ARRAY_BUFFER, vertices.size() * sizeof(float), &vertices[0], GL_STATIC_DRAW);
        glVertexAttribPointer(0, 3, GL_FLOAT, GL_FALSE, 0, 0);
        glEnableVertexAttribArray(0);
        
        glGenBuffers(1, &shape.vbo_normal);
        glBindBuffer(GL_ARRAY_BUFFER, shape.vbo_normal);
        glBufferData(GL_ARRAY_BUFFER, normals.size() * sizeof(float), &normals[0], GL_STATIC_DRAW);
        glVertexAttribPointer(1, 3, GL_FLOAT, GL_FALSE, 0, 0);
        glEnableVertexAttribArray(1);

        glGenBuffers(1, &shape.vbo_texcoord);
        glBindBuffer(GL_ARRAY_BUFFER, shape.vbo_texcoord);
        glBufferData(GL_ARRAY_BUFFER, texcoords.size() * sizeof(float), &texcoords[0], GL_STATIC_DRAW);
        glVertexAttribPointer(2, 2, GL_FLOAT, GL_FALSE, 0, 0);
        glEnableVertexAttribArray(2);
        
        glGenBuffers(1, &shape.vbo_insPos);
        glBindBuffer(GL_ARRAY_BUFFER, shape.vbo_insPos);
        glBufferData(GL_ARRAY_BUFFER, this->m_numPlantInstance[index] * sizeof(float) * 3, this->m_plantInstancePositions[index], GL_STATIC_DRAW);
        glVertexAttribPointer(3, 3, GL_FLOAT, GL_FALSE, 0, 0);
        glVertexAttribDivisor(3, 2);
        glEnableVertexAttribArray(3);

        glGenBuffers(1, &shape.ibo);
        glBindBuffer(GL_ELEMENT_ARRAY_BUFFER, shape.ibo);
        glBufferData(GL_ELEMENT_ARRAY_BUFFER, indices.size() * sizeof(float), &indices[0], GL_STATIC_DRAW);
        
        shape.materialID = mesh->mMaterialIndex;
        shape.drawCount = mesh->mNumFaces * 3;
        // save shape...
        shapes.push_back(shape);
    }
    aiReleaseImport(scene);
    model.shapes = shapes;
    return model;
}

////////////////////////////////////////////////////////////////////////////////////////////////////////////////
ObjManager::ObjManager() {
    
    // Load model
    Model house = LoadModels("models/medievalHouse.obj");
    house.position = glm::vec3(631, 130, 468);
    house.rotation = glm::vec3(0.0, 60.0, 0.0);
    this->models.push_back(house);
    house.position = glm::vec3(656, 135, 483);
    house.rotation = glm::vec3(0.0, 15.0, 0.0);
    this->models.push_back(house);
    
    // setup program
    this->shader = new Shader("shader/vertex_normal.vs.glsl", "shader/fragment_normal.fs.glsl");
    this->shader->useShader();
    GLuint programId = this->shader->getProgramID();
    this->uniform.m_mat = glGetUniformLocation(programId, "m_matrix");
    this->uniform.v_mat = glGetUniformLocation(programId, "v_matrix");
    this->uniform.p_mat = glGetUniformLocation(programId, "p_matrix");
    this->uniform.view_pos = glGetUniformLocation(programId, "view_pos");
    this->uniform.tex = glGetUniformLocation(programId, "tex_color");
    this->uniform.normal_tex = glGetUniformLocation(programId, "tex_normal");
    this->uniform.flag = glGetUniformLocation(programId, "flag");
    this->uniform.clip_plane = glGetUniformLocation(programId, "plane");
    this->uniform.renderBlur = glGetUniformLocation(programId, "renderBlur");
    glUniform1i(this->uniform.tex, 3);
    glUniform1i(this->uniform.normal_tex, 4);
    
    /* shadow */
    this->uniform.um4shadow_LOC = glGetUniformLocation(programId, "um4shadow");
    this->uniform.tex_shadow = glGetUniformLocation(programId, "tex_shadow");
    glUniform1i(this->uniform.tex_shadow, 5);
    /* defer */
    this->uniform.Ka_LOC = glGetUniformLocation(programId, "Ka");
    this->uniform.Kd_LOC = glGetUniformLocation(programId, "Kd");
    this->uniform.Ks_LOC = glGetUniformLocation(programId, "Ks");
    
    this->uniform.pointLightOpen_LOC = glGetUniformLocation(programId, "pointLightOpen");
}

ObjManager::Model ObjManager::LoadModels(std::string filePath)
{
    Model model;
    const aiScene *scene = aiImportFile(filePath.c_str(), aiProcess_CalcTangentSpace | aiProcessPreset_TargetRealtime_MaxQuality);
    if(!scene) std::cout << "Fail load scene " + filePath+"\n";
    // load material texture
    for (unsigned int i = 0; i < scene->mNumMaterials; ++i)
    {
        aiMaterial *material = scene->mMaterials[i];
        Material Material;
        aiString texturePath;
        if (material->GetTexture(aiTextureType_DIFFUSE, 0, &texturePath) == aiReturn_SUCCESS)
        {
            Material.diff_tex = new TextureMaterial(std::string(texturePath.C_Str()));
            Material.diff_tex->setTextureUnit(GL_TEXTURE3);
            if (Material.diff_tex) std::cout << "Load diff tex "+std::string(texturePath.C_Str())+"\n";
        }
        if (material->GetTexture(aiTextureType_HEIGHT, 0, &texturePath) == aiReturn_SUCCESS)
        {
            Material.normal_tex = new TextureMaterial(std::string(texturePath.C_Str()));
            Material.normal_tex->setTextureUnit(GL_TEXTURE4);
            if (Material.normal_tex) std::cout << "Load normal tex "+std::string(texturePath.C_Str())+"\n";
        }
        else
            std::cout << "Load texture fail "+std::string(texturePath.C_Str())+"\n";
        /* defer */
        aiColor3D color;
        material->Get(AI_MATKEY_COLOR_AMBIENT, color);
        Material.Ka = glm::vec4(color.r, color.g, color.b, 1.0);
        material->Get(AI_MATKEY_COLOR_DIFFUSE, color);
        Material.Kd = glm::vec4(color.r, color.g, color.b, 1.0);
        material->Get(AI_MATKEY_COLOR_SPECULAR, color);
        Material.Ks = glm::vec4(color.r, color.g, color.b, 1.0);
        model.materials.push_back(Material);
    }

    std::vector<Shape> shapes;
    for (unsigned int i = 0; i < scene->mNumMeshes; ++i)
    {
        aiMesh *mesh = scene->mMeshes[i];
        Shape shape;
        glGenVertexArrays(1, &shape.vao);
        glBindVertexArray(shape.vao);

        std::vector<float> vertices, texcoords, normals, tangents;
        // create 3 vbos to hold data
        for (unsigned int v = 0; v < mesh->mNumVertices; ++v)
        {
            vertices.push_back(mesh->mVertices[v][0]);
            vertices.push_back(mesh->mVertices[v][1]);
            vertices.push_back(mesh->mVertices[v][2]);
            texcoords.push_back(mesh->mTextureCoords[0][v][0]);
            texcoords.push_back(1-mesh->mTextureCoords[0][v][1]);
            normals.push_back(mesh->mNormals[v][0]);
            normals.push_back(mesh->mNormals[v][1]);
            normals.push_back(mesh->mNormals[v][2]);
            tangents.push_back(mesh->mTangents[v][0]);
            tangents.push_back(mesh->mTangents[v][1]);
            tangents.push_back(mesh->mTangents[v][2]);
        }
        std::vector<int> indices;
        // create 1 ibo to hold data
        for (unsigned int f = 0; f < mesh->mNumFaces; ++f)
        {
            indices.push_back(mesh->mFaces[f].mIndices[0]);
            indices.push_back(mesh->mFaces[f].mIndices[1]);
            indices.push_back(mesh->mFaces[f].mIndices[2]);
            
        }

        // glVertexAttribPointer / glEnableVertexArray calls...
        glGenBuffers(1, &shape.vbo_position);
        glBindBuffer(GL_ARRAY_BUFFER, shape.vbo_position);
        glBufferData(GL_ARRAY_BUFFER, vertices.size() * sizeof(float), &vertices[0], GL_STATIC_DRAW);
        glVertexAttribPointer(0, 3, GL_FLOAT, GL_FALSE, 0, 0);
        glEnableVertexAttribArray(0);

        glGenBuffers(1, &shape.vbo_normal);
        glBindBuffer(GL_ARRAY_BUFFER, shape.vbo_normal);
        glBufferData(GL_ARRAY_BUFFER, normals.size() * sizeof(float), &normals[0], GL_STATIC_DRAW);
        glVertexAttribPointer(1, 3, GL_FLOAT, GL_FALSE, 0, 0);
        glEnableVertexAttribArray(1);

        glGenBuffers(1, &shape.vbo_tangent);
        glBindBuffer(GL_ARRAY_BUFFER, shape.vbo_tangent);
        glBufferData(GL_ARRAY_BUFFER, normals.size() * sizeof(float), &tangents[0], GL_STATIC_DRAW);
        glVertexAttribPointer(2, 3, GL_FLOAT, GL_FALSE, 0, 0);
        glEnableVertexAttribArray(2);

        glGenBuffers(1, &shape.vbo_texcoord);
        glBindBuffer(GL_ARRAY_BUFFER, shape.vbo_texcoord);
        glBufferData(GL_ARRAY_BUFFER, texcoords.size() * sizeof(float), &texcoords[0], GL_STATIC_DRAW);
        glVertexAttribPointer(3, 2, GL_FLOAT, GL_FALSE, 0, 0);
        glEnableVertexAttribArray(3);
        

        glGenBuffers(1, &shape.ibo);
        glBindBuffer(GL_ELEMENT_ARRAY_BUFFER, shape.ibo);
        glBufferData(GL_ELEMENT_ARRAY_BUFFER, indices.size() * sizeof(float), &indices[0], GL_STATIC_DRAW);

        shape.materialID = mesh->mMaterialIndex;
        shape.drawCount = mesh->mNumFaces * 3;
        // save shape...
        shapes.push_back(shape);
    }
    aiReleaseImport(scene);
    model.shapes = shapes;
    return model;
}


void ObjManager::render(Camera *camera, GLuint fboDataTexture_shadow, glm::mat4 shadow_sbpv_matrix, glm::vec4 plane, bool blur, bool pointLightOpen)
{
    this->shader->useShader();
    glm::mat4 proj_matrix = camera->proj_matirx;
    glm::mat4 view_matrix = camera->view_matrix;
    for (int i = 0; i < this->models.size(); i++)
    {
        glm::mat4 T, S, R;
        T = glm::translate(glm::mat4(1.0), this->models[i].position);
        S = glm::scale(glm::mat4(1.0), this->models[i].scale);
        R = glm::rotate(glm::mat4(1.0), this->models[i].rotation.y, glm::vec3(0.0, 1.0, 0.0));
        glm::mat4 model_matrix = T * R * S;

        glUniformMatrix4fv(this->uniform.m_mat, 1, GL_FALSE, &model_matrix[0][0]);
        glUniformMatrix4fv(this->uniform.v_mat, 1, GL_FALSE, &view_matrix[0][0]);
        glUniformMatrix4fv(this->uniform.p_mat, 1, GL_FALSE, &proj_matrix[0][0]);
        glUniform4fv(this->uniform.clip_plane, 1, &plane[0]);
        glUniform1i(this->uniform.tex, 3);
        glUniform1i(this->uniform.normal_tex, 4);
        glUniform1i(this->uniform.renderBlur, blur);
        glUniform1i(this->uniform.flag, this->flag);
        glUniform3fv(this->uniform.view_pos, 1, &camera->position[0]);
        glUniform1i(this->uniform.pointLightOpen_LOC, pointLightOpen);
        
        /* ********** shadow ************************************************** */
        glUniform1i(this->uniform.tex_shadow, 5);
        glActiveTexture(GL_TEXTURE5);
        glBindTexture(GL_TEXTURE_2D, fboDataTexture_shadow);
        
        glm::mat4 shadow_matrix = shadow_sbpv_matrix * model_matrix;
        glUniformMatrix4fv(this->uniform.um4shadow_LOC, 1, GL_FALSE, value_ptr(shadow_matrix));
        /* ******************************************************************** */
        
        for (int j = 0 ; j < this->models[i].shapes.size(); j++)
        {
            glBindVertexArray(this->models[i].shapes[j].vao);
            this->models[i].materials[this->models[i].shapes[j].materialID].diff_tex->bind();
            this->models[i].materials[this->models[i].shapes[j].materialID].normal_tex->bind();
            /* defer */
            glUniform4fv(this->uniform.Ka_LOC, 1, &this->models[i].materials[this->models[i].shapes[j].materialID].Ka[0]);
            glUniform4fv(this->uniform.Kd_LOC, 1, &this->models[i].materials[this->models[i].shapes[j].materialID].Kd[0]);
            glUniform4fv(this->uniform.Ks_LOC, 1, &this->models[i].materials[this->models[i].shapes[j].materialID].Ks[0]);
            glBindBuffer(GL_ELEMENT_ARRAY_BUFFER, this->models[i].shapes[j].ibo);
            glDrawElements(GL_TRIANGLES, this->models[i].shapes[j].drawCount, GL_UNSIGNED_INT, 0);
        }
    }
}
void ObjManager::normalSwitch()
{
    this->flag = !this->flag;
}

ObjManager::~ObjManager(){}
/////////////////////////////////////////////////////////////////////////////////////////////////////

AirPlaneManager::AirPlaneManager() {
    
    // Load model
    this->models.push_back(LoadModels("models/airplane.obj"));
    
    // setup program
    this->shader = new Shader("shader/vertex_airplane.vs.glsl", "shader/fragment_airplane.fs.glsl");
    this->shader->useShader();
    GLuint programId = this->shader->getProgramID();
    
    this->uniform.um4m_LOC = glGetUniformLocation(programId, "um4m");
    this->uniform.um4v_LOC = glGetUniformLocation(programId, "um4v");
    this->uniform.um4p_LOC = glGetUniformLocation(programId, "um4p");
    this->uniform.um4shadow_LOC = glGetUniformLocation(programId, "um4shadow");
    this->uniform.clip_plane = glGetUniformLocation(programId, "plane");
    this->uniform.renderBlur = glGetUniformLocation(programId, "renderBlur");
    /* defer */
    this->uniform.Ka_LOC = glGetUniformLocation(programId, "Ka");
    this->uniform.Kd_LOC = glGetUniformLocation(programId, "Kd");
    this->uniform.Ks_LOC = glGetUniformLocation(programId, "Ks");
    
    this->uniform.tex = glGetUniformLocation(programId, "tex");
    glUniform1i(this->uniform.tex, 3);
    this->uniform.tex_shadow = glGetUniformLocation(programId, "tex_shadow");
    glUniform1i(this->uniform.tex_shadow, 4);
    
    this->uniform.pointLightOpen_LOC = glGetUniformLocation(programId, "pointLightOpen");

}

void AirPlaneManager::render(Camera *camera, glm::vec3 position, glm::mat4 rot_mat, GLuint fboDataTexture_shadow, glm::mat4 shadow_sbpv_matrix, glm::vec4 plane, bool blur, bool pointLightOpen)
{
    this->shader->useShader();
    glm::mat4 proj_matrix = camera->proj_matirx;
    glm::mat4 view_matrix = camera->view_matrix;

    glm::mat4 T, S, R;
    T = glm::translate(glm::mat4(1.0), position);
    S = glm::scale(glm::mat4(1.0), this->models[0].scale);
    R = rot_mat;
    glm::mat4 model_matrix = T * R * S;

    glUniformMatrix4fv(this->uniform.um4m_LOC, 1, GL_FALSE, value_ptr(model_matrix));
    glUniformMatrix4fv(this->uniform.um4v_LOC, 1, GL_FALSE, value_ptr(view_matrix));
    glUniformMatrix4fv(this->uniform.um4p_LOC, 1, GL_FALSE, value_ptr(proj_matrix));
    
    glUniform1i(this->uniform.tex, 3);
    glUniform1i(this->uniform.renderBlur, blur);
    glUniform4fv(this->uniform.clip_plane, 1, &plane[0]);
    glUniform1i(this->uniform.pointLightOpen_LOC, pointLightOpen);
    
    /* ********** shadow ************************************************** */
    glUniform1i(this->uniform.tex_shadow, 4);
    glActiveTexture(GL_TEXTURE4);
    glBindTexture(GL_TEXTURE_2D, fboDataTexture_shadow);
    
    glm::mat4 shadow_matrix = shadow_sbpv_matrix * model_matrix;
    glUniformMatrix4fv(this->uniform.um4shadow_LOC, 1, GL_FALSE, value_ptr(shadow_matrix));
    /* ******************************************************************** */
    
    for (int j = 0 ; j < this->models[0].shapes.size(); j++)
    {
        glBindVertexArray(this->models[0].shapes[j].vao);
        this->models[0].materials[this->models[0].shapes[j].materialID].diff_tex->bind();
        /* defer */
        glUniform4fv(this->uniform.Ka_LOC, 1, &this->models[0].materials[this->models[0].shapes[j].materialID].Ka[0]);
        glUniform4fv(this->uniform.Kd_LOC, 1, &this->models[0].materials[this->models[0].shapes[j].materialID].Kd[0]);
        glUniform4fv(this->uniform.Ks_LOC, 1, &this->models[0].materials[this->models[0].shapes[j].materialID].Ks[0]);
        
        glBindBuffer(GL_ELEMENT_ARRAY_BUFFER, this->models[0].shapes[j].ibo);
        glDrawElements(GL_TRIANGLES, this->models[0].shapes[j].drawCount, GL_UNSIGNED_INT, 0);
    }
}

AirPlaneManager::Model AirPlaneManager::LoadModels(std::string filePath)
{
    Model model;
    const aiScene *scene = aiImportFile(filePath.c_str(), aiProcessPreset_TargetRealtime_MaxQuality);
    if(!scene) std::cout << "Fail load scene " + filePath+"\n";
    // load material texture
    for (unsigned int i = 0; i < scene->mNumMaterials; ++i)
    {
        aiMaterial *material = scene->mMaterials[i];
        AirPlaneManager::Material Material;
        aiString texturePath;
        if (material->GetTexture(aiTextureType_DIFFUSE, 0, &texturePath) == aiReturn_SUCCESS)
        {
            Material.diff_tex = new TextureMaterial(std::string(texturePath.C_Str()));
            Material.diff_tex->setTextureUnit(GL_TEXTURE3);
            std::cout << "Load diff tex "+std::string(texturePath.C_Str())+"\n";
        }
        else
            std::cout << "Load texture fail "+std::string(texturePath.C_Str())+"\n";
        /* defer */
        aiColor3D color;
        material->Get(AI_MATKEY_COLOR_AMBIENT, color);
        Material.Ka = glm::vec4(color.r, color.g, color.b, 1.0);
        material->Get(AI_MATKEY_COLOR_DIFFUSE, color);
        Material.Kd = glm::vec4(color.r, color.g, color.b, 1.0);
        material->Get(AI_MATKEY_COLOR_SPECULAR, color);
        Material.Ks = glm::vec4(color.r, color.g, color.b, 1.0);
        model.materials.push_back(Material);
    }

    std::vector<Shape> shapes;
    for (unsigned int i = 0; i < scene->mNumMeshes; ++i)
    {
        aiMesh *mesh = scene->mMeshes[i];
        Shape shape;
        glGenVertexArrays(1, &shape.vao);
        glBindVertexArray(shape.vao);

        std::vector<float> vertices, texcoords, normals;
        // create 3 vbos to hold data
        for (unsigned int v = 0; v < mesh->mNumVertices; ++v)
        {
            vertices.push_back(mesh->mVertices[v][0]);
            vertices.push_back(mesh->mVertices[v][1]);
            vertices.push_back(mesh->mVertices[v][2]);
            texcoords.push_back(mesh->mTextureCoords[0][v][0]);
            texcoords.push_back(1-mesh->mTextureCoords[0][v][1]);
            normals.push_back(mesh->mNormals[v][0]);
            normals.push_back(mesh->mNormals[v][1]);
            normals.push_back(mesh->mNormals[v][2]);
        }
        std::vector<int> indices;
        // create 1 ibo to hold data
        for (unsigned int f = 0; f < mesh->mNumFaces; ++f)
        {
            indices.push_back(mesh->mFaces[f].mIndices[0]);
            indices.push_back(mesh->mFaces[f].mIndices[1]);
            indices.push_back(mesh->mFaces[f].mIndices[2]);
        }

        // glVertexAttribPointer / glEnableVertexArray calls...
        glGenBuffers(1, &shape.vbo_position);
        glBindBuffer(GL_ARRAY_BUFFER, shape.vbo_position);
        glBufferData(GL_ARRAY_BUFFER, vertices.size() * sizeof(float), &vertices[0], GL_STATIC_DRAW);
        glVertexAttribPointer(0, 3, GL_FLOAT, GL_FALSE, 0, 0);
        glEnableVertexAttribArray(0);

        glGenBuffers(1, &shape.vbo_normal);
        glBindBuffer(GL_ARRAY_BUFFER, shape.vbo_normal);
        glBufferData(GL_ARRAY_BUFFER, normals.size() * sizeof(float), &normals[0], GL_STATIC_DRAW);
        glVertexAttribPointer(1, 3, GL_FLOAT, GL_FALSE, 0, 0);
        glEnableVertexAttribArray(1);

        glGenBuffers(1, &shape.vbo_texcoord);
        glBindBuffer(GL_ARRAY_BUFFER, shape.vbo_texcoord);
        glBufferData(GL_ARRAY_BUFFER, texcoords.size() * sizeof(float), &texcoords[0], GL_STATIC_DRAW);
        glVertexAttribPointer(2, 2, GL_FLOAT, GL_FALSE, 0, 0);
        glEnableVertexAttribArray(2);

        glGenBuffers(1, &shape.ibo);
        glBindBuffer(GL_ELEMENT_ARRAY_BUFFER, shape.ibo);
        glBufferData(GL_ELEMENT_ARRAY_BUFFER, indices.size() * sizeof(float), &indices[0], GL_STATIC_DRAW);

        shape.materialID = mesh->mMaterialIndex;
        shape.drawCount = mesh->mNumFaces * 3;
        // save shape...
        shapes.push_back(shape);
    }
    aiReleaseImport(scene);
    model.shapes = shapes;
    return model;
}
AirPlaneManager::~AirPlaneManager(){}


/////////////////////////////////////////////////

WindowManager::WindowManager(){
    this->shader = new Shader("shader/vertex_window.vs.glsl", "shader/fragment_window.fs.glsl");
    this->shader->useShader();
    GLuint programId = this->shader->getProgramID();
    this->uniform.tex = glGetUniformLocation(programId, "tex");
    glUniform1i(this->uniform.tex, 0);
    
    this->CreateWindowVAO();
};

void WindowManager::CreateWindowVAO()
{
    glGenVertexArrays(1, &this->window.vao);
    glBindVertexArray(this->window.vao);
    static const GLfloat window_vertex[] = {
        //vec2 position vec2 texture_coord
        1.0f, -1.0f, 1.0f, 0.0f,
       -1.0f, -1.0f, 0.0f, 0.0f,
       -1.0f,  1.0f, 0.0f, 1.0f,
        1.0f, 1.0f, 1.0f, 1.0f };

    glGenBuffers(1, &this->window.vbo);
    glBindBuffer(GL_ARRAY_BUFFER, this->window.vbo);
    glBufferData(GL_ARRAY_BUFFER, sizeof(window_vertex), window_vertex, GL_STATIC_DRAW);
    glVertexAttribPointer(0, 2, GL_FLOAT, GL_FALSE, sizeof(GL_FLOAT)*4, 0);
    glVertexAttribPointer(1, 2, GL_FLOAT, GL_FALSE, sizeof(GL_FLOAT)*4,
                          (const GLvoid*)(sizeof(GL_FLOAT)*2));
    glEnableVertexAttribArray( 0 );
    glEnableVertexAttribArray( 1 );
}

void WindowManager::render(GLuint tex, int screenWidth, int screenHeight)
{
    glBindFramebuffer(GL_DRAW_FRAMEBUFFER, 0);
    glDrawBuffer(GL_COLOR_ATTACHMENT0);
    glViewport(0, 0, screenWidth, screenHeight);
    glClear(GL_COLOR_BUFFER_BIT | GL_DEPTH_BUFFER_BIT);
    glClearColor(0.0f, 0.0f, 0.0f, 1.0f);
    
    this->shader->useShader();
    glUniform1i(this->uniform.tex, 3);
    glActiveTexture(GL_TEXTURE3);
    glBindTexture(GL_TEXTURE_2D, tex);
    glBindVertexArray(this->window.vao);
    glDrawArrays(GL_TRIANGLE_FAN, 0, 4);
}
WindowManager::~WindowManager(){};

/////////////////////////////////////////////////////////////
SphereManager::SphereManager() {
    
    // Load model
    model = LoadModels("models/Sphere.obj");
    
    // setup program
    this->shader = new Shader("shader/vertex_bloom.vs.glsl", "shader/fragment_bloom.fs.glsl");
    this->shader->useShader();
    GLuint programId = this->shader->getProgramID();
    
    this->uniform.m_mat = glGetUniformLocation(programId, "m_mat");
    this->uniform.v_mat = glGetUniformLocation(programId, "v_mat");
    this->uniform.p_mat = glGetUniformLocation(programId, "p_mat");
    this->uniform.renderBlur = glGetUniformLocation(programId, "renderBlur");

}

void SphereManager::render(Camera *camera, bool renderBlur)
{
    this->shader->useShader();

    glm::mat4 T, S, R;
    T = glm::translate(glm::mat4(1.0), model.position);
    S = glm::scale(glm::mat4(1.0), model.scale);
    R = glm::rotate(glm::mat4(1.0), deg2rad(model.rotation.y), glm::vec3(0.0, 1.0, 0.0));
    glm::mat4 model_matrix = T * R * S;

    glUniformMatrix4fv(uniform.m_mat, 1, GL_FALSE, value_ptr(model_matrix));
    glUniformMatrix4fv(uniform.v_mat, 1, GL_FALSE, value_ptr(camera->view_matrix));
    glUniformMatrix4fv(uniform.p_mat, 1, GL_FALSE, value_ptr(camera->proj_matirx));
    glUniform1i(uniform.renderBlur, renderBlur);

    
   
    for (int j = 0 ; j < model.shapes.size(); j++)
    {
        glBindVertexArray(model.shapes[j].vao);
        glBindBuffer(GL_ELEMENT_ARRAY_BUFFER, model.shapes[j].ibo);
        glDrawElements(GL_TRIANGLES, model.shapes[j].drawCount, GL_UNSIGNED_INT, 0);
    }
}

SphereManager::Model SphereManager::LoadModels(std::string filePath)
{
    Model model;
    const aiScene *scene = aiImportFile(filePath.c_str(), aiProcessPreset_TargetRealtime_MaxQuality);
    if(!scene) std::cout << "Fail load scene " + filePath+"\n";

    std::vector<Shape> shapes;
    for (unsigned int i = 0; i < scene->mNumMeshes; ++i)
    {
        aiMesh *mesh = scene->mMeshes[i];
        Shape shape;
        glGenVertexArrays(1, &shape.vao);
        glBindVertexArray(shape.vao);

        std::vector<float> vertices, texcoords, normals;
        // create 3 vbos to hold data
        for (unsigned int v = 0; v < mesh->mNumVertices; ++v)
        {
            vertices.push_back(mesh->mVertices[v][0]);
            vertices.push_back(mesh->mVertices[v][1]);
            vertices.push_back(mesh->mVertices[v][2]);
            texcoords.push_back(mesh->mTextureCoords[0][v][0]);
            texcoords.push_back(1-mesh->mTextureCoords[0][v][1]);
            normals.push_back(mesh->mNormals[v][0]);
            normals.push_back(mesh->mNormals[v][1]);
            normals.push_back(mesh->mNormals[v][2]);
        }
        std::vector<int> indices;
        // create 1 ibo to hold data
        for (unsigned int f = 0; f < mesh->mNumFaces; ++f)
        {
            indices.push_back(mesh->mFaces[f].mIndices[0]);
            indices.push_back(mesh->mFaces[f].mIndices[1]);
            indices.push_back(mesh->mFaces[f].mIndices[2]);
        }

        // glVertexAttribPointer / glEnableVertexArray calls...
        glGenBuffers(1, &shape.vbo_position);
        glBindBuffer(GL_ARRAY_BUFFER, shape.vbo_position);
        glBufferData(GL_ARRAY_BUFFER, vertices.size() * sizeof(float), &vertices[0], GL_STATIC_DRAW);
        glVertexAttribPointer(0, 3, GL_FLOAT, GL_FALSE, 0, 0);
        glEnableVertexAttribArray(0);

        glGenBuffers(1, &shape.vbo_normal);
        glBindBuffer(GL_ARRAY_BUFFER, shape.vbo_normal);
        glBufferData(GL_ARRAY_BUFFER, normals.size() * sizeof(float), &normals[0], GL_STATIC_DRAW);
        glVertexAttribPointer(1, 3, GL_FLOAT, GL_FALSE, 0, 0);
        glEnableVertexAttribArray(1);

        glGenBuffers(1, &shape.vbo_texcoord);
        glBindBuffer(GL_ARRAY_BUFFER, shape.vbo_texcoord);
        glBufferData(GL_ARRAY_BUFFER, texcoords.size() * sizeof(float), &texcoords[0], GL_STATIC_DRAW);
        glVertexAttribPointer(2, 2, GL_FLOAT, GL_FALSE, 0, 0);
        glEnableVertexAttribArray(2);

        glGenBuffers(1, &shape.ibo);
        glBindBuffer(GL_ELEMENT_ARRAY_BUFFER, shape.ibo);
        glBufferData(GL_ELEMENT_ARRAY_BUFFER, indices.size() * sizeof(float), &indices[0], GL_STATIC_DRAW);

        shape.drawCount = mesh->mNumFaces * 3;
        // save shape...
        shapes.push_back(shape);
    }
    aiReleaseImport(scene);
    model.shapes = shapes;
    return model;
}
SphereManager::~SphereManager(){}
