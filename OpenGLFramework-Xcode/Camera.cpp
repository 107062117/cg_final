#include "Camera.h"
#include<vector>

Camera::Camera()
{
    
}

Camera:: ~Camera()
{
    
}

void Camera::InitProjectionMatrix(float fov, float w, float h, float n, float f)
{
    width = w;
    height = h;
    near = n;
    far = f;
    proj_matirx = glm::perspective(glm::radians(fov), w/h, near, far);
}


void Camera::InitViewMatrix(glm::vec3 pos, glm::vec3 lookat)
{
    position = pos;
    lookAt = lookat;
    glm::vec3 front = pos - lookat;
    distance = glm::distance(pos, lookat);
    pitch = rad2deg(-atan2(front.y, sqrt(front.z * front.z + front.x *front.x)));
    yaw = rad2deg(atan2(front.x, front.z)) - 90.0;
    SetViewMatrix();
}

void Camera::SetViewMatrix()
{
    view_matrix = glm::lookAt(position, lookAt, glm::vec3(0.0, 1.0, 0.0));
}



void Camera::Rotate(float yaw_offset, float pitch_offset)
{
    yaw += yaw_offset;
    pitch += pitch_offset;
    if (pitch > 89.0f)
        pitch = 89.0f;
    if (pitch < -89.0f)
        pitch = -89.0f;
    glm::vec3 front = glm::vec3(cos(deg2rad(yaw))*cos(deg2rad(pitch)), sin(deg2rad(pitch)), sin(deg2rad(yaw)) * cos(deg2rad(pitch)) );
    lookAt = position + glm::normalize(front) * distance;
    SetViewMatrix();
}
void Camera::Move(float speed)
{
    glm::vec3 front = glm::normalize(lookAt - position);
    position += front * speed;
    lookAt += front * speed;
    SetViewMatrix();
}

