#pragma once

#include <fstream>
#include <tuple>
#include <vector>
#include <iostream>
#include <GLM/mat4x4.hpp>
#include <GLM/gtx/transform.hpp>
#include <GLM/gtc/type_ptr.hpp>
#include "assimp/cimport.h"
#include "assimp/scene.h"
#include "assimp/postprocess.h"
#include "TextureMaterial.h"
#include "Shader.h"
#include "Camera.h"
/* defer */
#include "DeferFrameBuffer.h"
#include "BloomFrameBuffer.h"

#ifdef _MSC_VER
    #pragma comment (lib, "glew32.lib")
    #pragma comment(lib, "freeglut.lib")
    #pragma comment(lib, "assimp.lib")
#endif

#define deg2rad(x) ((x)*((3.1415926f)/(180.0f)))
#define rad2deg(x) ((x)*(180.0f)/(3.1415926f))


class PlantManager {
public:
    struct Shape
    {
        GLuint vao;
        GLuint vbo_position;
        GLuint vbo_normal;
        GLuint vbo_texcoord;
        GLuint vbo_insPos;
        GLuint ibo;
        int drawCount;
        int materialID;
    };

    struct Material
    {
        TextureMaterial *diff_tex;
        /* defer */
        glm::vec4 Ka;
        glm::vec4 Kd;
        glm::vec4 Ks;
    };

    struct Model
    {
        glm::vec3 position = glm::vec3(470.0, 120.0, 450.0);
        glm::vec3 scale = glm::vec3(1.0, 1.0, 1.0);
        glm::vec3 rotation = glm::vec3(90.0, 0.0, 0.0);
        std::vector<Shape> shapes;
        std::vector<Material> materials;
    };
    std::vector<Model> models;
    Shader *grass_shader, *tree_shader;
    struct
    {
        GLuint m_mat;
        GLuint v_mat;
        GLuint p_mat;
        GLuint clip_plane;
        GLuint tex;
        /* shadow */
        GLuint tex_shadow;
        GLuint um4shadow_LOC;
        GLuint light_pos_LOC;
        GLuint renderBlur;
        /* defer */
        GLuint Ka_LOC;
        GLuint Kd_LOC;
        GLuint Ks_LOC;
        /* bloom */
        GLint pointLightOpen_LOC;
        
    }tree_uniform;
    struct
    {
        GLuint m_mat;
        GLuint v_mat;
        GLuint p_mat;
        GLuint clip_plane;
        GLuint tex;
        /* shadow */
        GLuint tex_shadow;
        GLuint um4shadow_LOC;
        GLuint renderBlur;
        /* defer */
        GLuint Ka_LOC;
        GLuint Kd_LOC;
        GLuint Ks_LOC;
        /* bloom */
        GLint pointLightOpen_LOC;
        
    }grass_uniform;
public:
    PlantManager();
    virtual ~PlantManager();

public:
    int m_numPlantInstance[4];
    float *m_plantInstancePositions[4];

public:
    static std::tuple<int, float*> appendPlants(const std::string &posFile);
    void render(Camera *cam, glm::vec4 plane, GLuint fboDataTexture_shadow, glm::mat4 shadow_sbpv_matrix, bool blur, bool pointLightOpen);
    Model LoadModels(std::string path, int index);
};
//////////////////////////////////////////////////////////
class ObjManager {
public:
    struct Shape
    {
        GLuint vao;
        GLuint vbo_position;
        GLuint vbo_normal;
        GLuint vbo_texcoord;
        GLuint vbo_tangent;
        GLuint ibo;
        int drawCount;
        int materialID;
    };

    struct Material
    {
        TextureMaterial *diff_tex;
        TextureMaterial *normal_tex;
        /* defer */
        glm::vec4 Ka;
        glm::vec4 Kd;
        glm::vec4 Ks;
        
    };

    struct Model
    {
        glm::vec3 position = glm::vec3(512.0, 110.0, 490.0);
        glm::vec3 scale = glm::vec3(1.0, 1.0, 1.0);
        glm::vec3 rotation = glm::vec3(0, 0, 0);
        std::vector<Shape> shapes;
        std::vector<Material> materials;
    };
    std::vector<Model> models;
    Shader *shader;
    bool flag = false;
    struct
    {
        GLuint m_mat;
        GLuint p_mat;
        GLuint v_mat;
        GLuint clip_plane;
        GLuint view_pos;
        GLuint tex;
        GLuint normal_tex;
        GLuint flag;
        GLuint tex_shadow;
        GLint um4shadow_LOC;
        GLuint renderBlur;
        /* defer */
        GLuint Ka_LOC;
        GLuint Kd_LOC;
        GLuint Ks_LOC;
        /* bloom */
        GLint pointLightOpen_LOC;
    }uniform;


public:
    ObjManager();
    virtual ~ObjManager();

public:

    void init();

    void render(Camera *cam, GLuint fboDataTexture_shadow, glm::mat4 shadow_sbpv_matrix, glm::vec4 plane, bool blur, bool pointLightOpen);

    Model LoadModels(std::string path);
    void normalSwitch();
};
/////////////////////////////////////////
class AirPlaneManager
{
public:
    struct Shape
    {
        GLuint vao;
        GLuint vbo_position;
        GLuint vbo_normal;
        GLuint vbo_texcoord;
        GLuint ibo;
        int drawCount;
        int materialID;
    };

    struct Material
    {
        TextureMaterial *diff_tex;
        /* defer */
        glm::vec4 Ka;
        glm::vec4 Kd;
        glm::vec4 Ks;
    };

    struct Model
    {
        glm::vec3 position = glm::vec3(512.0, 110.0, 500.0);
        glm::vec3 scale = glm::vec3(1.0, 1.0, 1.0);
        glm::vec3 rotation = glm::vec3(0, 0, 0);
        std::vector<Shape> shapes;
        std::vector<Material> materials;
    };
    std::vector<Model> models;
    Shader *shader;
    struct
    {
        GLuint clip_plane;
        GLuint tex;
        GLint um4m_LOC;
        GLint um4v_LOC;
        GLint um4p_LOC;
        /* shadow */
        GLuint tex_shadow;
        GLuint um4shadow_LOC;
        GLuint renderBlur;
        /* defer */
        GLuint Ka_LOC;
        GLuint Kd_LOC;
        GLuint Ks_LOC;
        /* bloom */
        GLint pointLightOpen_LOC;
    }uniform;

public:
    AirPlaneManager();
    virtual ~AirPlaneManager();

public:

    void render(Camera *cam, glm::vec3 position, glm::mat4 rot_mat, GLuint fboDataTexture_shadow, glm::mat4 shadow_sbpv_matrix, glm::vec4 plane, bool blur, bool pointLightOpen);

    Model LoadModels(std::string path);
};

//////////////////////////////////////////////////////////
class WindowManager
{
public:
    Shader *shader;
    
    struct
    {
        GLuint tex;
    }uniform;

public:
    WindowManager();
    virtual ~WindowManager();
public:
    struct
    {
        GLuint vao;
        GLuint vbo;
    }window;
    void CreateWindowVAO();
    void render(GLuint tex, int screenWidth, int screenHeight);
};
////////////////////////////////////////////////////////////////

class SphereManager
{
public:
    struct Shape
    {
        GLuint vao;
        GLuint vbo_position;
        GLuint vbo_normal;
        GLuint vbo_texcoord;
        GLuint ibo;
        int drawCount;
    };

    struct Model
    {
        glm::vec3 position = glm::vec3(636.48, 134.79, 495.98);
        glm::vec3 scale = glm::vec3(4.0, 4.0, 4.0);
        glm::vec3 rotation = glm::vec3(0, 0, 0);
        std::vector<Shape> shapes;
    };
    Model model;
    Shader *shader;
    struct
    {
        GLint m_mat;
        GLint v_mat;
        GLint p_mat;
        GLuint renderBlur;
    }uniform;
    
public:
    SphereManager();
    virtual ~SphereManager();

public:

    void render(Camera *cam, bool renderBlur);

    Model LoadModels(std::string path);
};
////////////////////////////////////////////////////////////////
